#include "TransformsDialog.h"
#include "d3dApp.h"
#include "WindowsDelegateMethods.h"

TransformsDialog::TransformsDialog()
{
}


TransformsDialog::~TransformsDialog()
{
	DestroyWindow(m_dialogHWND);
}

void TransformsDialog::Init()
{

}

BOOL CALLBACK TransformsDialog::DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
    switch (message) 
    { 
        case WM_INITDIALOG: 
            //CheckDlgButton(hwndDlg, ID_ABSREL, fRelative); 
			CheckRadioButton(hwndDlg, IDC_RADIO2, IDC_RADIO4, IDC_RADIO2);
            return TRUE; 
 
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
            { 
                case IDOK: 
					return TRUE;
 
                case IDCANCEL: 
                    DestroyWindow(hwndDlg); 
                    WDM->ToggleTransforms();
					delete this;
                    return TRUE; 
            } 
		//case
    } 

    return FALSE; 
} 