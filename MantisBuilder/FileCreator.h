#pragma once
#include <string>
#include <vector>

class FileCreator
{
private:

	void CopyDirectory(std::string DirectoryToCopy, std::string DirectoryToCopyTo);
	int CountFiles(std::string path);

public:
	FileCreator();
	~FileCreator();

	void HandleFiles(std::vector<std::string> IncomingFiles, std::string directory);
};

