#pragma once
#include <vector>
#include "MemoryManager.h"

#define WMI WorldManager::Instance()

class WorldManager
{
private:
	MemoryManager* m_memory;

	WorldManager();

public:
	static WorldManager* Instance();
	~WorldManager();

	void CleanUp();

	void onResetDevice();
	void onLostDevice();

	std::vector<std::vector<MantisMesh*>*> getDrawableMeshes();

};