#include "WindowsDelegateMethods.h"
#include "MemoryManager.h"
#include "JSONParser.h"
#include "resource.h"

#include "AssetDialog.h"
#include "LevelsDialog.h"
#include "NodesDialog.h"
#include "PropertyDialog.h"
#include "TransformsDialog.h"
#include "ZonesDialog.h"
#include "NewDialog.h"
#include "ProgressDialog.h"

#include "LevelFactory.h"
#include "Actor.h"
#include "WorkBench.h"
#include "Camera.h"

using namespace std;

WindowsDelegateMethods::WindowsDelegateMethods(void)
{
	m_assetsOpen	= false;
	m_LevelLoaded	= false;
	m_solutionOpen	= false;
	m_propertyOpen	= false;
	m_progressOpen	= false;
}

WindowsDelegateMethods* WindowsDelegateMethods::Instance()
{
	static WindowsDelegateMethods temp = WindowsDelegateMethods();
	return &temp;
}

WindowsDelegateMethods::~WindowsDelegateMethods(void)
{
	
}

BOOL WindowsDelegateMethods::msgProc(HWND DialogWindow, UINT message, WPARAM wParam, LPARAM lParam)
{	
	BOOL result = FALSE;

	if(m_assetsOpen || (lParam == 101) || (m_DialogInitalizing == 11)) //Jesus FUCKING Christ
		if((m_DialogInitalizing == 11) || (DialogWindow == m_AssetDialog->getHWND()))
			result = m_AssetDialog->DialogProc(DialogWindow, message, wParam, lParam);

	if(result) return TRUE;

	if(m_levelsOpen|| (lParam == 102) || (m_DialogInitalizing == 12))
		if((m_DialogInitalizing == 12) || (DialogWindow == m_LevelsDialog->getHWND()))
			result = m_LevelsDialog->DialogProc(DialogWindow, message, wParam, lParam);

	if(result) return TRUE;

	if(m_propertyOpen || (lParam == 104) || (m_DialogInitalizing == 14))
		if((m_DialogInitalizing == 14) || (DialogWindow == m_PropertyDialog->getHWND()))
			result = m_PropertyDialog->DialogProc(DialogWindow, message, wParam, lParam);

	if(result) return TRUE;

	if(m_transformsOpen || (lParam == 105) || (m_DialogInitalizing == 15))
		if((m_DialogInitalizing == 15) || (DialogWindow == m_TransformsDialog->getHWND()))
		result = m_TransformsDialog->DialogProc(DialogWindow, message, wParam, lParam);

	if(result) return TRUE;

	if(m_newOpen || (lParam == 107) || (m_DialogInitalizing == 17))
		if((m_DialogInitalizing == 17) || (DialogWindow == m_NewDialog->getHWND()))
			result = m_NewDialog->DialogProc(DialogWindow, message, wParam, lParam);

	if(result) return TRUE;

	if(m_progressOpen || (lParam == 108) || (m_DialogInitalizing == 18))
		if((m_DialogInitalizing == 18) || (DialogWindow == m_ProgressDialog->getHWND()))
			result = m_ProgressDialog->DialogProc(DialogWindow, message, wParam, lParam);

	if(result) return TRUE;

	return FALSE;
}

//===================================================
//
// Delegate Functions
//
//===================================================

// Private Functions
//===================================================
void WindowsDelegateMethods::ToggleCaptionChange()
{
	if(m_HeaderChanged)
	{
		SetWindowText(m_owningHandle, m_ProgramCaption.c_str());
	} else {
		string newTitle = m_ProgramCaption + "*";
		SetWindowText(m_owningHandle, newTitle.c_str());
	}
	m_HeaderChanged = !m_HeaderChanged;
}

void WindowsDelegateMethods::SetCaptionPrimary(string primary)
{
	SetWindowText(m_owningHandle, (LPSTR)primary.c_str());
	m_ProgramCaption = primary;
}

void WindowsDelegateMethods::SetCaptionSecondary(string secondary)
{
	string newTitle = m_SolutionName + " - " + secondary;
	SetWindowText(m_owningHandle, (LPSTR)newTitle.c_str());
	m_ProgramCaption = newTitle;
}

void WindowsDelegateMethods::SaveCurrentLevel()
{
	if(m_LoadedLevelPath == "") return;

	JSONParser parser;

	auto Schematics = MMI->getSchematics();
	auto ActorTypes = MMI->getGraphicsObjects();

	parser.CreateNewFile(m_LoadedLevelPath);

	//if there are no schematics there can be no level file
	if(Schematics.size() == 0)
	{
		parser.CloseFile();
		return;
	}

	parser.CreateArray("Schematics");

	for(auto schematic : Schematics)
	{
		parser.CreateObject();

			parser.CreateVariable("Type", "Mesh");
			parser.CreateVariable("fileName", schematic.second.fileName);
			parser.CreateVariable("Name", schematic.second.name);

			parser.CreateObject("Textures");

			//CHANGE THIS TO READ IN ACTUAL TEXTURE FILE NAMES
				parser.CreateVariable("Diffuse", schematic.second.mapNames[0]);
				parser.CreateVariable("Bump", schematic.second.mapNames[1]);
				parser.CreateVariable("Normal", schematic.second.mapNames[2]);
				parser.CreateVariable("Specular", schematic.second.mapNames[3]);
				parser.CreateVariable("Illumination", schematic.second.mapNames[4]);

			parser.CloseObject();

		parser.CloseObject();
	}

	parser.CloseArray();

	parser.CreateArray("Actors");

	for(auto type : ActorTypes)
	{
		for(auto actor : type.second)
		{
			parser.CreateObject();

				parser.CreateVariable("Schematic", actor->m_schematicName);
				parser.CreateVariable("Position", Vector3ToString(actor->m_position));
				parser.CreateVariable("Orientation", "0.0, 0.0, 0.0,");
				parser.CreateVariable("Scale", Vector3ToString(actor->m_scale));

			parser.CloseObject();
		}
	}

	parser.CloseArray();

	parser.CloseFile();
}

void WindowsDelegateMethods::SaveSolution(string FileName)
{
	JSONParser parser;

	parser.CreateNewFile(FileName);

	parser.CreateVariable("SolutionName", m_SolutionName);
	parser.CreateVariable("SolutionDirectory", m_SolutionFilePath);

	RECT rect;

	parser.CreateObject("Assets");

	if(m_assetsOpen)
	{
		GetWindowRect(m_AssetDialog->getHWND(), &rect);
		char buffer[22];
		parser.CreateVariable("Open", "1");
		parser.CreateVariable("xPos", _itoa(rect.left, buffer, 10));
		parser.CreateVariable("yPos", _itoa(rect.top, buffer, 10));

	} else {
		parser.CreateVariable("Open", "0");
		parser.CreateVariable("xPos", "0");
		parser.CreateVariable("yPos", "0");
	}
	parser.CloseObject();

	parser.CreateObject("Transforms");

	if(m_transformsOpen)
	{
		GetWindowRect(m_TransformsDialog->getHWND(), &rect);
		char buffer[22];
		parser.CreateVariable("Open", "1");
		parser.CreateVariable("xPos", _itoa(rect.left, buffer, 10));
		parser.CreateVariable("yPos", _itoa(rect.top, buffer, 10));
	} else {
		parser.CreateVariable("Open", "0");
		parser.CreateVariable("xPos", "0");
		parser.CreateVariable("yPos", "0");
	}
	parser.CloseObject();

	parser.CreateObject("Levels");

	if(m_levelsOpen)
	{
		GetWindowRect(m_LevelsDialog->getHWND(), &rect);
		char buffer[22];
		parser.CreateVariable("Open", "1");
		parser.CreateVariable("xPos", _itoa(rect.left, buffer, 10));
		parser.CreateVariable("yPos", _itoa(rect.top, buffer, 10));
	} else {
		parser.CreateVariable("Open", "0");
		parser.CreateVariable("xPos", "0");
		parser.CreateVariable("yPos", "0");
	}
	parser.CloseObject();

	parser.CreateObject("Properties");

	if(m_propertyOpen)
	{
		GetWindowRect(m_PropertyDialog->getHWND(), &rect);
		char buffer[22];
		parser.CreateVariable("Open", "1");
		parser.CreateVariable("xPos", _itoa(rect.left, buffer, 10));
		parser.CreateVariable("yPos", _itoa(rect.top, buffer, 10));
	} else {
		parser.CreateVariable("Open", "0");
		parser.CreateVariable("xPos", "0");
		parser.CreateVariable("yPos", "0");
	}
	parser.CloseObject();

	parser.CloseFile();
}

void WindowsDelegateMethods::LoadSolution(string filename)
{
	JSONParser parser;

	parser.OpenFile(filename);

	JSONObject inputFile = parser.GetParsedObject();

	if(inputFile.m_objects.find("Assets")->second.m_variables.find("Open")->second == "1")
	{
		ToggleAssets();
		SetWindowPos(m_AssetDialog->getHWND(),
					 NULL,
					 atoi(inputFile.m_objects.find("Assets")->second.m_variables.find("xPos")->second.c_str()),
					 atoi(inputFile.m_objects.find("Assets")->second.m_variables.find("yPos")->second.c_str()),
					 0,
					 0,
					 SWP_NOSIZE);
	}

	if(inputFile.m_objects.find("Transforms")->second.m_variables.find("Open")->second == "1")
	{
		ToggleTransforms();
		SetWindowPos(m_TransformsDialog->getHWND(),
					 NULL,
					 atoi(inputFile.m_objects.find("Transforms")->second.m_variables.find("xPos")->second.c_str()),
					 atoi(inputFile.m_objects.find("Transforms")->second.m_variables.find("yPos")->second.c_str()),
					 0,
					 0,
					 SWP_NOSIZE);
	}

	if(inputFile.m_objects.find("Levels")->second.m_variables.find("Open")->second == "1")
	{
		ToggleLevels();
		SetWindowPos(m_LevelsDialog->getHWND(),
					 NULL,
					 atoi(inputFile.m_objects.find("Levels")->second.m_variables.find("xPos")->second.c_str()),
					 atoi(inputFile.m_objects.find("Levels")->second.m_variables.find("yPos")->second.c_str()),
					 0,
					 0,
					 SWP_NOSIZE);
	}

	if(inputFile.m_objects.find("Properties")->second.m_variables.find("Open")->second == "1")
	{
		ToggleProperty();
		SetWindowPos(m_PropertyDialog->getHWND(),
					 NULL,
					 atoi(inputFile.m_objects.find("Properties")->second.m_variables.find("xPos")->second.c_str()),
					 atoi(inputFile.m_objects.find("Properties")->second.m_variables.find("yPos")->second.c_str()),
					 0,
					 0,
					 SWP_NOSIZE);
	}

	//m_WorkingDirectory = inputFile.m_variables.find("SolutionDirectory")->second; redundant

	m_SolutionName = inputFile.m_variables.find("SolutionName")->second;

	SetCaptionPrimary(m_SolutionName);
}

OPENFILENAME WindowsDelegateMethods::OpenFileDialog(char* extension, char* Filter, char* fileName)
{
	OPENFILENAME ofn;

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;
	ofn.lpstrFile = fileName;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = 256;
	ofn.lpstrDefExt = "mb";
	ofn.lpstrFilter = "Mantis Builder File\0*.MB\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

	return ofn;
}

string WindowsDelegateMethods::GetFilePath(string input)
{
	for(auto it = input.rbegin();
		it != input.rend();
		it++)
	{	
		if(*it == '\\')
		{
			it++;
			return string(input.begin(), it.base());
		}
	}

	return "";
}

string WindowsDelegateMethods::GetFileNameFromPath(string path)
{
	for(auto it = path.rbegin();
		it != path.rend();
		it++)
	{	
		if(*it == '\\')
		{
			it++;
			return string(it.base(), path.end());
		}
	}

	return "";
}

// Public Functions
//===================================================
void WindowsDelegateMethods::InitDelegate(HINSTANCE appInst, HWND appHandle, DLGPROC procFunc, string caption)
{
	m_owningInstance	= appInst;
	m_owningHandle		= appHandle;
	m_owningDialogProc	= procFunc;
	m_ProgramCaption	= caption;
}

void WindowsDelegateMethods::Unload()
{
	delete m_AssetDialog;
	delete m_LevelsDialog;
	delete m_NodesDialog;
	delete m_PropertyDialog;
	delete m_TransformsDialog;
	delete m_ZonesDialog;
	delete m_NewDialog;
	delete m_ProgressDialog;
}

void WindowsDelegateMethods::New()
{
	ToggleNew();
}

void WindowsDelegateMethods::NewSolution(string name)
{
	char FileName[256];

	if(m_SolutionFilePath == "")
	{
		OPENFILENAME ofn = OpenFileDialog("mb", "Mantis Builder File\0*.MB\0", FileName);

		GetSaveFileName(&ofn);

		m_SolutionFilePath = FileName;
	}

	m_WorkingDirectory = GetFilePath(m_SolutionFilePath);

	SaveSolution(FileName);

	m_solutionOpen = true;
	m_SolutionName = name;
	SetCaptionPrimary(name);
}

void WindowsDelegateMethods::Save()
{
	SaveCurrentLevel();

	if(m_SolutionFilePath == "")
	{
		SaveAs();
		return;
	}

	SaveSolution(m_SolutionFilePath);

	if(m_HeaderChanged)
		ToggleCaptionChange();
}

void WindowsDelegateMethods::SaveAs()
{
	SaveCurrentLevel();

	char FileName[256];
	OPENFILENAME ofn = OpenFileDialog("mb", "Mantis Builder File\0*.MB\0", FileName);

	GetSaveFileName(&ofn);

	SaveSolution(FileName);

	if(m_HeaderChanged)
		ToggleCaptionChange();
}

void WindowsDelegateMethods::Open()
{
	char FileName[256];
	OPENFILENAME ofn = OpenFileDialog("mb", "Mantis Builder File\0*.MB\0", FileName);

	GetOpenFileName(&ofn);

	m_SolutionFilePath = FileName;

	if(m_SolutionFilePath == "")
		return;

	m_WorkingDirectory = GetFilePath(m_SolutionFilePath);

	m_solutionOpen = true;

	LoadSolution(FileName);
}

void WindowsDelegateMethods::Export()
{

}

void WindowsDelegateMethods::Cut()
{

}

void WindowsDelegateMethods::Copy()
{

}

void WindowsDelegateMethods::Paste()
{

}

void WindowsDelegateMethods::Documentation()
{

}

void WindowsDelegateMethods::About()
{

}

void WindowsDelegateMethods::LevelChange()
{
	if(!m_HeaderChanged)
		ToggleCaptionChange();
}

bool WindowsDelegateMethods::SolutionIsOpen()
{
	return m_solutionOpen;
}

string WindowsDelegateMethods::WorkingDirectory()
{
	return m_WorkingDirectory;
}

void WindowsDelegateMethods::LoadLevelFromFile(string file, string name)
{
	if(m_HeaderChanged)
	{
		int response = MessageAlert("Warning", 
			                        "Current level has not been saved!\nSave now?", 
									MB_YESNOCANCEL | MB_ICONEXCLAMATION, 
									MB_ICONERROR, 
									true);


		if(response == IDCANCEL) return;
		if(response == IDYES) Save();
	}

	LevelFactory factory;

	if(m_LevelLoaded)
	{
		MMI->CleanUp();
		MMI->InitMemory();
		WBI->Reset();
		factory.BuildLevel(file);
	}
	else
	{
		factory.BuildLevel(file);
		m_LevelLoaded = true;
	}

	m_LoadedLevelPath = file;
	SetCaptionSecondary(name);
}

void WindowsDelegateMethods::NewLevel(string filename, string levelname, string description, int size)
{
	if(m_HeaderChanged)
	{
		int response = MessageAlert("Warning", 
			                        "Current level has not been saved!\nSave now?", 
									MB_YESNOCANCEL | MB_ICONEXCLAMATION, 
									MB_ICONERROR, 
									true);


		if(response == IDCANCEL) return;
		if(response == IDYES) Save();
	}

	//check for extension
	bool pass = false;
	for(auto it = filename.rbegin();
		it != filename.rend();
		it++)
	{
		if(*it == '.')
		{
			string test = string(it.base(), filename.end());
			if(test != "json")
				filename = string(filename.begin(), it.base()) + "json";
			pass = true;
			break;
		}
	}

	if(!pass)
		filename = filename += ".json";

	m_LoadedLevelPath = m_WorkingDirectory + "\\Assets\\Levels\\" + filename;
	SetCaptionSecondary(levelname);

	LevelFactory factory;

	if(m_LevelLoaded)
	{
		MMI->CleanUp();
		MMI->InitMemory();
		WBI->Reset();

		SaveCurrentLevel(); //create the blank file

		factory.BuildLevel(m_LoadedLevelPath);
	}
	else
	{
		SaveCurrentLevel(); //create the blank file

		factory.BuildLevel(m_LoadedLevelPath);
		m_LevelLoaded = true;
	}

}

bool WindowsDelegateMethods::LevelIsLoaded()
{
	return m_LevelLoaded;
}

void WindowsDelegateMethods::UpdateProperties()
{
	if(m_propertyOpen)
		m_PropertyDialog->Update();
}

void WindowsDelegateMethods::CreateActor()
{
	Actor *newActor = new Actor();

	MMI->RegisterNewMeshSchematic("Placeholder", "placeholderMesh", m_WorkingDirectory+"\\");

	newActor->Graphics = MMI->CreateMantisMesh("Placeholder");

	newActor->Graphics->m_position = gCamera->GetFixedPoint();

	WBI->SetSelectedActor(newActor);
	MMI->AddActor(newActor);
}

//***************************************************

// Public Utility Functions
//===================================================

void WindowsDelegateMethods::NewPickedItem(string path, string fileName, string itemName)
{
	m_pickedItem.path = path;
	m_pickedItem.fileName = fileName;
	m_pickedItem.itemName = itemName;

	//Assign to selected Mesh
	if(WBI->getSelectedActor() != nullptr)
		MMI->SwapMesh(WBI->getSelectedActor()->Graphics, itemName, fileName, m_WorkingDirectory + "\\");
}

int WindowsDelegateMethods::MessageAlert(string header, string message, DWORD behavior, DWORD soundType, bool sound)
{
	if(sound)
		MessageBeep(soundType);

	int result = MessageBox(m_owningHandle, message.c_str(), header.c_str(), behavior);

	return result;
}

void WindowsDelegateMethods::ProgressBar(int elementCount, string operation)
{
	ToggleProgressBar();
	m_ProgressDialog->SetItemCount(elementCount);
	m_ProgressDialog->SetOperationText(operation);
}

void WindowsDelegateMethods::ProgressBarUpdate(string task)
{
	m_ProgressDialog->SetTaskText(task);
	m_ProgressDialog->Step();
}

void WindowsDelegateMethods::ProgressBarShutdown()
{
	m_ProgressDialog->Close();
}

//***************************************************


//===================================================
//
// Asset Dialog Functions
//
//===================================================

void WindowsDelegateMethods::ToggleAssets()
{
	if(!m_assetsOpen)
	{
		m_AssetDialog = new AssetDialog();
		m_DialogInitalizing = 11;
		HWND temp = CreateDialogParam(m_owningInstance,
										MAKEINTRESOURCE(IDD_DIALOG1),
										m_owningHandle,
										m_owningDialogProc,
										101);
		m_AssetDialog->setHWND(temp);
		m_DialogInitalizing = 0;

		ShowWindow(m_AssetDialog->getHWND(), SW_SHOW);
	}

	m_assetsOpen = !m_assetsOpen;
}

bool WindowsDelegateMethods::assetsIsActive()
{
	return m_assetsOpen;
}

//***********************************************

//===================================================
//
// Levels Dialog Functions
//
//===================================================

void WindowsDelegateMethods::ToggleLevels()
{
	if(!m_levelsOpen)
	{
		m_LevelsDialog = new LevelsDialog();
		m_DialogInitalizing = 12;

		HWND temp = CreateDialogParam(m_owningInstance,
										MAKEINTRESOURCE(IDD_DIALOG4),
										m_owningHandle,
										m_owningDialogProc,
										102);
		m_LevelsDialog->setHWND(temp);
		m_DialogInitalizing = 0;

		ShowWindow(m_LevelsDialog->getHWND(), SW_SHOW);
	}

	m_levelsOpen = !m_levelsOpen;
}

bool WindowsDelegateMethods::LevelsIsActive()
{
	return m_levelsOpen;
}

//***************************************************

//===================================================
//
// Nodes Dialog Functions
//
//===================================================



//***************************************************

//===================================================
//
// Property Dialog Functions
//
//===================================================

void WindowsDelegateMethods::ToggleProperty()
{
	if(!m_propertyOpen)
	{
		m_PropertyDialog = new PropertyDialog();
		m_DialogInitalizing = 14;

		HWND temp = CreateDialogParam(m_owningInstance,
									  MAKEINTRESOURCE(IDD_DIALOG5),
									  m_owningHandle,
									  m_owningDialogProc,
									  104);

		m_PropertyDialog->setHWND(temp);
		m_DialogInitalizing = 0;

		ShowWindow(temp, SW_SHOW);
	}

	m_propertyOpen = !m_propertyOpen;
}

bool WindowsDelegateMethods::PropertyIsActive()
{
	return m_propertyOpen;
}

//***************************************************

//===================================================
//
// Transforms Dialog Functions
//
//===================================================

void WindowsDelegateMethods::ToggleTransforms()
{
	if(!m_transformsOpen)
	{
		m_TransformsDialog = new TransformsDialog();
		m_DialogInitalizing = 15;

		HWND temp = CreateDialogParam(m_owningInstance,
													 MAKEINTRESOURCE(IDD_DIALOG3),
													 m_owningHandle,
													 m_owningDialogProc,
													 105);

		m_TransformsDialog->setHWND(temp);
		m_DialogInitalizing = 0;

		ShowWindow(temp, SW_SHOW);
	}

	m_transformsOpen = !m_transformsOpen;
}

bool WindowsDelegateMethods::TransformsIsActive()
{
	return m_transformsOpen;
}

//***************************************************

//===================================================
//
// Zones Dialog Functions
//
//===================================================



//***************************************************

//===================================================
//
// New Dialog Functions
//
//===================================================

void WindowsDelegateMethods::ToggleNew()
{
	if(!m_newOpen)
	{
		m_NewDialog = new NewDialog();
		m_DialogInitalizing = 17;
		HWND temp = CreateDialogParam(m_owningInstance,
									  MAKEINTRESOURCE(IDD_DIALOG2),
									  m_owningHandle,
									  m_owningDialogProc,
									  107);

		m_NewDialog->setHWND(temp);
		m_DialogInitalizing = 0;

		ShowWindow(temp, SW_SHOW);
	}

	m_newOpen = !m_newOpen;
}

bool WindowsDelegateMethods::NewIsActive()
{
	return m_newOpen;
}

//***************************************************


//===================================================
//
// Progress Bar Dialog Functions
//
//===================================================

void WindowsDelegateMethods::ToggleProgressBar()
{
	if(!m_progressOpen)
	{
		m_ProgressDialog = new ProgressDialog();
		m_DialogInitalizing = 18;
		HWND temp = CreateDialogParam(m_owningInstance,
									  MAKEINTRESOURCE(IDD_DIALOG6),
									  m_owningHandle,
									  m_owningDialogProc,
									  108);

		m_ProgressDialog->setHWND(temp);
		m_DialogInitalizing = 0;

		ShowWindow(temp, SW_SHOW);
	}

	m_progressOpen = !m_progressOpen;
}

bool WindowsDelegateMethods::ProgressBarIsActive()
{
	return m_progressOpen;
}

//***************************************************


//===================================================
//
// ##### Dialog Functions
//
//===================================================



//***************************************************