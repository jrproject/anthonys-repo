#pragma once
#include "d3dUtil.h"

class MantisMesh;

struct AnchorHelperElement{
	MantisMesh	*m_helper;
	char		m_direction;
};

enum CurrentAnchor{
	MODE_NONE = 0,
	MODE_TRANSLATE,
	MODE_SCALE,
	MODE_ROTATE,
};

class HelperAnchor
{
private:

	void LoadAssets();

private:
	D3DXVECTOR3 *m_attatchPoint;
	std::vector<AnchorHelperElement> m_anchors;
	std::vector<D3DXVECTOR3> m_rotations;
	std::vector<D3DXVECTOR3> m_baises;
	int m_mode;

public:
	HelperAnchor();
	~HelperAnchor();

	void Update();

	void SetMode(int newMode);
	void SetAttachPoint(D3DXVECTOR3 *newPoint);

	int ClickedOn(D3DXVECTOR3 &originW, D3DXVECTOR3 &dirW);
};

