#pragma once
#include "d3dUtil.h"

class MantisMesh
{
private:

public:
	MantisMesh(MeshSchematic Schematic)
	{
		//Read Schematic
		m_mesh			= Schematic.mesh;
		m_renderFlags	= Schematic.detailFlags;

		m_maps[0]		= Schematic.maps[0];
		m_maps[1]		= Schematic.maps[1];
		m_maps[2]		= Schematic.maps[2];
		m_maps[3]		= Schematic.maps[3];
		m_maps[4]		= Schematic.maps[4];

		//m_Diffuse			= (Schematic.detailFlags & DIF) ? true : false;
		//m_Bump			= (Schematic.detailFlags & BMP) ? true : false;
		//m_Normal			= (Schematic.detailFlags & NRM) ? true : false;
		//m_Specular		= (Schematic.detailFlags & SPC) ? true : false;
		//m_Illumination	= (Schematic.detailFlags & ILL) ? true : false;
		//~Read Schematic

		//init standard variables
		m_position		= D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		m_parent		= NULL;
		m_scale			= D3DXVECTOR3(1.0f, 1.0f, 1.0f);
		D3DXQuaternionIdentity(&m_orientation);

		m_material.ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		m_material.diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
		m_material.spec = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f);
		m_material.specPower = 50.0f;

		m_AABB		= Schematic.BoundingBox;
		m_Sphere	= Schematic.BoundingSphere;

		///Level editor only
		m_isSelected = false;

		m_schematicName		= Schematic.name;
		m_fileName			= Schematic.fileName;
	}

	int					m_owningActorID;

	D3DXVECTOR3			m_position;
	MantisMesh			*m_parent;
	ID3DXMesh			*m_mesh;
	LPDIRECT3DTEXTURE9	m_maps[5];
	D3DXQUATERNION		m_orientation;

	D3DXMATRIX			m_transformMatrix;

	DWORD				m_renderFlags;

	D3DXVECTOR3			m_scale;

	AABB				*m_AABB;
	BoundingSphere		*m_Sphere;

	MeshMaterial		m_material;

	//EDITOIR SPECIAL ONLY MOTHER FUCKER - Jesus Walt, they're minerals
	bool				m_isSelected;
	std::string			m_schematicName;
	std::string			m_fileName;

	//D3DXMATRIX			m_transformToWorld;
	//D3DXMATRIX			m_transformToParent;

	//render flags
	//bool				m_Diffuse;
	//bool				m_Bump;
	//bool				m_Normal;
	//bool				m_Specular;
	//bool				m_Illumination;

	void buildTransformMatrix(){

		D3DXMATRIX R, T, S;
		//d3dx
		//D3DXQuaternionRotationAxis(
		D3DXQuaternionNormalize(&m_orientation, &m_orientation);
		D3DXMatrixRotationQuaternion(&R, &m_orientation);

		D3DXMatrixTranslation(&T, m_position.x, m_position.y, m_position.z);

		D3DXMatrixScaling(&S, m_scale.x, m_scale.y, m_scale.z);

		m_transformMatrix = S * R * T;

		//D3DXMATRIX O, R, T;
		//D3DXVECTOR3 p;

		//D3DXMatrixRotationQuaternion(&O, &m_orientation);
		//D3DXMatrixTranslation(&T, m_position.x, m_position.y, m_position.z);
		//m_transformToParent = O * T;

		//if(!m_parent == NULL)
		//{
		//	D3DXMatrixIsIdentity(&m_transformToWorld);


		//}
	}
};