#include "SkyBox.h"
#include "d3dUtil.h"
#include "Camera.h"

SkyBox::SkyBox(const std::string& filename, float radius)
{
	m_radius = radius;

	HR(D3DXCreateSphere(gd3dDevice, radius, 30, 30, &m_sphere, 0));
	HR(D3DXCreateCubeTextureFromFile(gd3dDevice, filename.c_str(), &m_cubeMap));

	ID3DXBuffer* error = 0;
	HR(D3DXCreateEffectFromFile(gd3dDevice, "Assets/Shaders/SkyBox.fx", 0, 0, 0, 0, &m_FX, &error));

	if(error)
		MessageBox(0, (char*)error->GetBufferPointer(), 0, 0);

	m_hTech   = m_FX->GetTechniqueByName("SkyTech");
	m_hWVP    = m_FX->GetParameterByName(0, "gWVP");
	m_hcube = m_FX->GetParameterByName(0, "gEnvMap");

	//these will not change
	HR(m_FX->SetTechnique(m_hTech));
	HR(m_FX->SetTexture(m_hcube, m_cubeMap));
}


SkyBox::~SkyBox()
{
	ReleaseCOM(m_sphere);
	ReleaseCOM(m_cubeMap);
	ReleaseCOM(m_FX);
}

void SkyBox::onLostDevice()
{
	HR(m_FX->OnLostDevice());
}

void SkyBox::onResetDevice()
{
	HR(m_FX->OnResetDevice());
}

void SkyBox::render()
{
	D3DXMATRIX W;
	D3DXVECTOR3 p = gCamera->pos();

	D3DXMatrixTranslation(&W, p.x, p.y, p.z);
	HR(m_FX->SetMatrix(m_hWVP, &(W * gCamera->viewProj())));

	UINT numPasses = 0;
	HR(m_FX->Begin(&numPasses, 0));
	HR(m_FX->BeginPass(0));
	HR(m_sphere->DrawSubset(0));
	HR(m_FX->EndPass());
	HR(m_FX->End());
}