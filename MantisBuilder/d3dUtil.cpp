#include "d3dUtil.h"
#include "Vertex.h"
#include <cassert>
#include <sstream>

//helper function from author
/*
	Although the meshes I'm using are properly created
	it's intresting to see the process of making them
	into proper meshes and optimizing them
*/
void LoadX(
	const std::string& filename, 
	ID3DXMesh** mesh,
	std::vector<Material>& materials, 
	std::vector<IDirect3DTexture9*>& textures)
{
	ID3DXMesh* meshSystem = 0;
	ID3DXBuffer* adjustmentBuffer = 0;
	ID3DXBuffer* materialBuffer = 0;
	DWORD numMaterials = 0;

	HR(D3DXLoadMeshFromX(filename.c_str(), 
		                 D3DXMESH_SYSTEMMEM, 
						 gd3dDevice, 
		                 &adjustmentBuffer, 
						 &materialBuffer, 0, 
						 &numMaterials, 
						 &meshSystem));

	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	HR(meshSystem->GetDeclaration(elems));

	bool hasNormals = false;
	D3DVERTEXELEMENT9 term = D3DDECL_END();
	for(int i = 0; i < MAX_FVF_DECL_SIZE; i++)
	{
		if(elems[i].Stream == 0xff)
			break;

		if( elems[i].Type == D3DDECLTYPE_FLOAT3 &&
			elems[i].Usage == D3DDECLUSAGE_NORMAL &&
			elems[i].UsageIndex == 0 )
		{
			hasNormals = true;
			break;
		}
	}

	D3DVERTEXELEMENT9 elements[64];
	UINT numElements = 0;
	VertexPNT::Decl->GetDeclaration(elements, &numElements);

	ID3DXMesh* temp = 0;
	HR(meshSystem->CloneMesh(D3DXMESH_SYSTEMMEM, 
		elements, gd3dDevice, &temp));
	ReleaseCOM(meshSystem);
	meshSystem = temp;

	if( hasNormals == false)
		HR(D3DXComputeNormals(meshSystem, 0));


	HR(meshSystem->Optimize(D3DXMESH_MANAGED | 
		D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, 
		(DWORD*)adjustmentBuffer->GetBufferPointer(), 0, 0, 0, mesh));
	ReleaseCOM(meshSystem); 
	ReleaseCOM(adjustmentBuffer); 


	if( materialBuffer != 0 && numMaterials != 0 )
	{
		D3DXMATERIAL* d3dxmtrls = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();

		for(DWORD i = 0; i < numMaterials; ++i)
		{
			// Save the ith material.  Note that the MatD3D property does not have an ambient
			// value set when its loaded, so just set it to the diffuse value.
			Material m;
			m.ambient		= d3dxmtrls[i].MatD3D.Diffuse;
			m.diffuse		= d3dxmtrls[i].MatD3D.Diffuse;
			m.specular      = d3dxmtrls[i].MatD3D.Specular;
			m.specularPower = d3dxmtrls[i].MatD3D.Power;
			materials.push_back( m );

			// Check if the ith material has an associative texture
			if( d3dxmtrls[i].pTextureFilename != 0 )
			{
				// Yes, load the texture for the ith subset
				IDirect3DTexture9* tex = 0;
				char* texFN = d3dxmtrls[i].pTextureFilename;
				HR(D3DXCreateTextureFromFile(gd3dDevice, texFN, &tex));

				// Save the loaded texture
				textures.push_back( tex );
			}
			else
			{
				// No texture for the ith subset
				textures.push_back( 0 );
			}
		}
	}
	ReleaseCOM(materialBuffer); // done w/ buffer
}

D3DXVECTOR3 QuatForwardVec(D3DXQUATERNION q)
{
	return D3DXVECTOR3( 2 * (q.x * q.z + q.w * q.y),
						2 * (q.y *q. x -q. w * q.x),
						1 - 2 * (q.x * q.x + q.y * q.y));
}

D3DXVECTOR3 QuatUpVec(D3DXQUATERNION q)
{
	return D3DXVECTOR3( 2 * (q.x * q.y - q.w * q.z),
						1 - 2 * (q.x * q.x + q.z * q.z),
						2 * (q.y * q.z + q.w * q.x));
}

D3DXVECTOR3 QuatRightVec(D3DXQUATERNION q)
{
	return D3DXVECTOR3( 1 - 2 * (q.y * q.y + q.z * q.z),
						2 * (q.x *q. y + q.w * q.z),
						2 * (q.x * q.z - q.w * q.y));
}

D3DXVECTOR3 StringToVector3(std::string line)
{
	float results[3];
	int i = 0;

	std::string::iterator index = line.begin();
	std::string value = "";

	for(; index != line.end(); index++)
	{
		if((*index == '\"') || (*index == ' '))
			continue;

		if(*index == ',')
		{
			results[i] = (float)atof(value.c_str());
			i++;
			value = "";
			continue;
		}
		value += *index;
	}
	

	assert((i != 2) && "THERE WAS AN ERROR CONVERTING STRING TO VEC3");

	return D3DXVECTOR3(results[0], results[1], results[2]);
}

std::string Vector3ToString(D3DXVECTOR3 vec)
{
	std::ostringstream ss;

	ss << vec.x << ", " << vec.y << ", " << vec.z << ",";

	return std::string(ss.str());
}