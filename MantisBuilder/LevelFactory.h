#pragma once
#include <string>
#include "JSONParser.h"

class LevelFactory
{
private:

	// Private Functions
	//=====================================
	void ReadSchematics();
	void CreateActors();
	//*************************************

private:

	JSONObject	m_JSONObject;

public:
	LevelFactory();
	~LevelFactory();

	// Public Functions
	//=====================================
	void BuildLevel(std::string filename);
	//*************************************
};

