#pragma once
#include <Windows.h>
#include <string>

class AssetDialog;
class LevelsDialog;
class NodesDialog;
class PropertyDialog;
class TransformsDialog;
class ZonesDialog;
class NewDialog;
class ProgressDialog;

#define WDM WindowsDelegateMethods::Instance()

struct PickedItem
{
	std::string path;
	std::string fileName;
	std::string itemName;
};

class WindowsDelegateMethods
{
private:

	WindowsDelegateMethods();

	// Private Delegate Functions
	//============================
	void ToggleCaptionChange();
	void SetCaptionPrimary(std::string primary);
	void SetCaptionSecondary(std::string secondary);

	void SaveCurrentLevel();
	void SaveSolution(std::string filename);

	void LoadSolution(std::string filename);

	OPENFILENAME OpenFileDialog(char* extension, char* Filter, char* fileName);
	std::string GetFilePath(std::string input);
	std::string GetFileNameFromPath(std::string path);
	//****************************

private:


	PickedItem			m_pickedItem;

	UINT				m_DialogInitalizing;

	HINSTANCE			m_owningInstance;
	HWND				m_owningHandle;
	DLGPROC				m_owningDialogProc;

	AssetDialog			*m_AssetDialog;
	LevelsDialog		*m_LevelsDialog;
	NodesDialog			*m_NodesDialog;
	PropertyDialog		*m_PropertyDialog;
	TransformsDialog	*m_TransformsDialog;
	ZonesDialog			*m_ZonesDialog;
	NewDialog			*m_NewDialog;
	ProgressDialog		*m_ProgressDialog;

	bool m_assetsOpen,
		 m_levelsOpen,
		 m_nodesOpen,
		 m_propertyOpen,
		 m_transformsOpen,
		 m_zonesOpen,
		 m_newOpen,
		 m_progressOpen;

	bool m_HeaderChanged;
	bool m_LevelLoaded;
	bool m_solutionOpen;

	std::string m_SolutionFilePath;
	std::string m_WorkingDirectory;
	std::string m_ProgramCaption;
	std::string m_SolutionName;
	std::string m_LoadedLevelPath;

public:
	static WindowsDelegateMethods* Instance();
	~WindowsDelegateMethods();

	//primary msg proc from winMain
	BOOL msgProc(HWND DialogWindow, UINT message, WPARAM wParam, LPARAM lParam);

	// Public Delegate Functions
	//============================
	void InitDelegate(HINSTANCE mainInstance, HWND mainHandle, DLGPROC procFunc, std::string caption);
	void Unload();

	void New();
	void NewSolution(std::string name);

	void Save();
	void SaveAs();
	void Open();
	void Export();

	void Cut();
	void Copy();
	void Paste();

	void Documentation();
	void About();

	void LevelChange();

	bool SolutionIsOpen();
	std::string WorkingDirectory();

	void LoadLevelFromFile(std::string file, std::string name);
	void NewLevel(std::string filename, std::string levelname, std::string description, int size);
	bool LevelIsLoaded();

	void UpdateProperties();

	void CreateActor();
	//****************************

	// Public Utility Functions
	//============================
	void NewPickedItem(std::string path, std::string fileName, std::string itemName);
	int MessageAlert(std::string header, std::string message, DWORD behavior, DWORD soundType, bool sound = false);
	void ProgressBar(int elements, std::string operation);
	void ProgressBarUpdate(std::string task = "");
	void ProgressBarShutdown();
	//****************************

	// Assets Dialog
	//=======================
	void ToggleAssets();
	bool assetsIsActive();
	//***********************


	// Levels Dialog
	//=======================
	void ToggleLevels();
	bool LevelsIsActive();
	//***********************


	// Nodes Dialog
	//=======================
	void ToggleNodes();
	bool NodesIsActive();
	//***********************


	// Property Dialog
	//=======================
	void ToggleProperty();
	bool PropertyIsActive();
	//***********************


	// Transforms Dialog
	//=======================
	void ToggleTransforms();
	bool TransformsIsActive();
	//***********************


	// Zones Dialog
	//=======================
	void ToggleZones();
	bool ZonesIsActive();
	//***********************


	// New Dialog
	//=======================
	void ToggleNew();
	bool NewIsActive();
	//***********************

	// Progress Bar Dialog
	//=======================
	void ToggleProgressBar();
	bool ProgressBarIsActive();
	//***********************
};

