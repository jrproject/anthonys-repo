#pragma once
#include "DialogWindow.h"

class NewDialog : public DialogWindow
{
private:

public:
	NewDialog();
	~NewDialog(void);

	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

