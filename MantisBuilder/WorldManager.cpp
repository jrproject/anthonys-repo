#include "WorldManager.h"

WorldManager* WorldManager::Instance()
{
	static WorldManager temp;
	return &temp;
}

WorldManager::WorldManager()
{

}


WorldManager::~WorldManager()
{
	//delete COM objects
}

void WorldManager::CleanUp()
{
	MMI->CleanUp();
}

//void WorldManager::RegisterCOMObjects(DWORD devices)
//{
//	//pull out flags ex: SPRITE | FONT 
//}

void WorldManager::onResetDevice()
{
	//m_memory->onResetDevice();
}

void WorldManager::onLostDevice()
{
	//m_memory->onLostDevice();
}

std::vector<std::vector<MantisMesh*>*> WorldManager::getDrawableMeshes()
{
	return MMI->GetGraphicsObjects();
}