#pragma once
#include "DialogWindow.h"

class TransformsDialog : public DialogWindow
{
private:

public:
	TransformsDialog();
	~TransformsDialog();

	virtual void Init();
	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

