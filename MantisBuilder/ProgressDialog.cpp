#include "ProgressDialog.h"
#include "WindowsDelegateMethods.h"
#include "d3dApp.h"
#include <CommCtrl.h>

using namespace std;

ProgressDialog::ProgressDialog(void)
{
}


ProgressDialog::~ProgressDialog(void)
{
}

// Public Functions
//=============================================

void ProgressDialog::SetOperationText(string text)
{
	HWND hwndList = GetDlgItem(m_dialogHWND, IDC_STATIC_OPERATION);
	SetWindowText(hwndList, text.c_str());
}

void ProgressDialog::SetTaskText(string text)
{
	HWND hwndList = GetDlgItem(m_dialogHWND, IDC_STATIC_TASK);
	SetWindowText(hwndList, text.c_str());
}

void ProgressDialog::SetItemCount(int count)
{
	HWND hwndList = GetDlgItem(m_dialogHWND, IDC_PROGRESS1);
	SendMessage(hwndList, PBM_SETRANGE, 0, MAKELPARAM(0, count));
	SendMessage(hwndList, PBM_SETSTEP, 1, 0);
}

void ProgressDialog::Step()
{
	HWND hwndList = GetDlgItem(m_dialogHWND, IDC_PROGRESS1);
	SendMessage(hwndList, PBM_STEPIT, 0, 0);
}

void ProgressDialog::Close()
{
	SendMessage(m_dialogHWND, WM_COMMAND, IDCANCEL, 0);
}

BOOL CALLBACK ProgressDialog::DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam)
{
	switch(messge)
	{
		case WM_INITDIALOG:
		{
			return TRUE;
		}

		case WM_COMMAND:
		{
			switch(LOWORD(wParam))
			{
				case IDCANCEL:
				{
					DestroyWindow(m_dialogHWND);
					WDM->ToggleProgressBar();
					delete this;
					return TRUE;
				}
			}
			return TRUE;
		}
	}
	return FALSE;
}