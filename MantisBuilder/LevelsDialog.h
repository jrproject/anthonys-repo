#pragma once
#include "DialogWindow.h"

struct LevelInfo{
	std::string m_location;
	std::string m_name;
	std::string m_description;
};

class LevelsDialog : public DialogWindow
{
private:
	std::vector<LevelInfo>	m_LevelFiles;
	std::string				m_workingDirectory;

public:
	LevelsDialog();
	~LevelsDialog();

	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

