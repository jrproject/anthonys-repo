#include "LevelsDialog.h"
#include "d3dApp.h"
#include "WindowsDelegateMethods.h"
#include <windowsx.h>

using namespace std;

struct Size {
	TCHAR sizeName[12];
	int size;
};

Size Sizes[] =
{
	{TEXT("SMALL"), 5000},
	{TEXT("MEDIUM"), 7000},
	{TEXT("LARGE"), 9000},
};

LevelsDialog::LevelsDialog(void)
{
}


LevelsDialog::~LevelsDialog(void)
{
	DestroyWindow(m_dialogHWND);
}

BOOL CALLBACK LevelsDialog::DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch(message)
	{
		case WM_INITDIALOG:
		{
			if(!WDM->SolutionIsOpen()) return TRUE;

			HWND hwndCombo = GetDlgItem(hwndDlg, IDC_COMBO1);
			for(int i = 0; i < ARRAYSIZE(Sizes); i++)
			{
				ComboBox_AddString(hwndCombo, Sizes[i].sizeName);
			}

			ComboBox_SetCurSel(hwndCombo, 0);

			m_workingDirectory = WDM->WorkingDirectory();

			//Always Create level folder
			CreateDirectory((m_workingDirectory + "\\Assets").c_str(), NULL);
			CreateDirectory((m_workingDirectory + "\\Assets\\Levels").c_str(), NULL);
			
			WIN32_FIND_DATA ffd;
			HANDLE hFind = INVALID_HANDLE_VALUE;
			string spec = m_workingDirectory + "\\Assets\\Levels\\*";

			hFind = FindFirstFile(spec.c_str(), &ffd);

			do {
				if((string(ffd.cFileName) != ".") && (string(ffd.cFileName) != "..")){
					if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
						//ignore files
					}
					else {
						LevelInfo level;
						level.m_name = ffd.cFileName;
						level.m_location = m_workingDirectory + "\\Assets\\Levels\\" + ffd.cFileName;
						level.m_description = "Temp Description";

						m_LevelFiles.push_back(level);
					}
				}
			} while (FindNextFile(hFind, &ffd) != 0);

			FindClose(hFind);

			HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST1);

			for(UINT i = 0; i < m_LevelFiles.size(); i++)
			{
				int pos = (int)SendMessage(hwndList, LB_ADDSTRING, 0, (LPARAM)m_LevelFiles[i].m_name.c_str());
				SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)i);
			}

			return TRUE;
		}

		case WM_COMMAND:
		{
			switch(LOWORD(wParam))
			{
				case IDCANCEL:
				{
					DestroyWindow(hwndDlg);
					WDM->ToggleLevels();
					delete this;
					return TRUE;
				}
				case IDC_BUTTON_ADDLEVEL: //ADD
				{
					string filename, levelname, description, buffer;
					int stringSize;
					buffer.resize(255);

					HWND hwnd = GetDlgItem(hwndDlg, IDC_EDIT1);
					stringSize = Edit_GetText(hwnd, (LPSTR)buffer.c_str(), 255);

					if(stringSize == 0)
					{
						WDM->MessageAlert("Error", 
										  "Please include the file name",
										  MB_OK | MB_ICONEXCLAMATION,
										  MB_ICONERROR,
										  true);

						return TRUE;
					}

					filename = buffer;
					filename.resize(stringSize);
					buffer.clear();
					buffer.resize(255);

					hwnd = GetDlgItem(hwndDlg, IDC_EDIT2);
					stringSize = Edit_GetText(hwnd, (LPSTR)buffer.c_str(), 255);

					
					if(stringSize == 0)
					{
						WDM->MessageAlert("Error", 
										  "Please include the level name",
										  MB_OK | MB_ICONEXCLAMATION,
										  MB_ICONERROR,
										  true);

						return TRUE;
					}

					levelname = buffer;
					levelname.resize(stringSize);
					buffer.clear();
					buffer.resize(255);

					hwnd = GetDlgItem(hwndDlg, IDC_EDIT3);
					stringSize = Edit_GetText(hwnd, (LPSTR)buffer.c_str(), 255);
					description = buffer;
					description.resize(stringSize);
					buffer.clear();
					
					hwnd = GetDlgItem(hwndDlg, IDC_COMBO1);
					stringSize = ComboBox_GetCurSel(hwnd);

					WDM->NewLevel(filename, levelname, description, stringSize); //0 LARGE, 1 MEDIUM, 2 SMALL

					return TRUE;
				}

				case IDC_BUTTON_SUBTRACTLEVEL: //SUBTRACT
				{
					int response = WDM->MessageAlert("Warning!",
									  "This action can not be reversed!\nContinue?",
									  MB_YESNO | MB_ICONEXCLAMATION, 
									  MB_ICONERROR, 
									  true);

					if(response == IDNO) return TRUE;

					HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST1);

					int lbItem = ListBox_GetCurSel(hwndList);
					int i = ListBox_GetItemData(hwndList, lbItem);

					DeleteFile(m_LevelFiles.at(i).m_location.c_str());

					m_LevelFiles.erase(m_LevelFiles.begin() + i); //erase from levels vector

					ListBox_ResetContent(hwndList);

					for(UINT i = 0; i < m_LevelFiles.size(); i++)
					{
						int pos = ListBox_AddString(hwndList, m_LevelFiles[i].m_name.c_str());
						ListBox_SetItemData(hwndList, pos, i);

						//int pos = (int)SendMessage(hwndList, LB_ADDSTRING, 0, (LPARAM)m_LevelFiles[i].m_name.c_str());
						//SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)i);
					}

					return TRUE;
				}

				case IDC_LIST1:
				{
					switch (HIWORD(wParam))
					{
						case LBN_SELCHANGE:
						{
							return TRUE;
						}

						case LBN_DBLCLK:
						{
							HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST1);

							int lbItem = (int)SendMessage(hwndList, LB_GETCURSEL, 0, 0);
							int i = (int)SendMessage(hwndList, LB_GETITEMDATA, lbItem, 0);

							WDM->LoadLevelFromFile(m_LevelFiles[i].m_location, m_LevelFiles[i].m_name);

							return TRUE;
						}
					}
					return TRUE;
				}
			}
			return TRUE;
		}
	}

	return FALSE;
}