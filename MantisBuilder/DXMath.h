#include <d3d9.h>
#include <d3dx9.h>

//class Quaternion;


namespace DXMath
{
	class Matrix4;

	class Vector3 : public D3DXVECTOR3
	{
	public:
		Vector3(D3DXVECTOR3 &vec3)							{ x = vec3.x; y = vec3.y; z = vec3.z;				}
		Vector3() : D3DXVECTOR3()							{ }
		Vector3(const float fx, 
				const float fy, 
				const float fz)								{ x = fx; y = fy; z = fz;							}
	public: 
		inline float	Length()							{ return D3DXVec3Length(this);						}
		inline float	LengthSq()							{ return D3DXVec3LengthSq(this);					}
		inline float	Dot(const Vector3 &b)				{ return D3DXVec3Dot(this, &b);						}

		inline Vector3	*Normalize()						{ return (Vector3*)D3DXVec3Normalize(this,this);	}
		inline Vector3	Cross(const Vector3 &b) const
		{
			Vector3 out;
			D3DXVec3Cross(&out, this, &b);
			return out;
		}
	};

	class Vector4 : public D3DXVECTOR4
	{
	public:
		Vector4(D3DXVECTOR4 &vec4)							{ x = vec4.x; y = vec4.y; z = vec4.z; w = vec4.w;	}
		Vector4() : D3DXVECTOR4()							{ }
		Vector4(const float fx, 
			    const float fy, 
				const float fz, 
				const float fw)								{ x = fx; y = fy; z = fz; w = fw;					}
		Vector4(const Vector3 &vec3)						{ x = vec3.x; y = vec3.y; z = vec3.z; w = 1.0f;		}
	public:
		inline float	Length()							{ D3DXVec4Length(this);								}
		inline float	LengthSq()							{ D3DXVec4LengthSq(this);							}
		inline float	Dot(const Vector4 &b)				{ D3DXVec4Dot(this, &b);							}

		inline Vector4	*Normalize()						{ return (Vector4*)D3DXVec4Normalize(this,this);	}
		//cross product would be D3DXVec3Cross()
	};

		class Quaternion : public D3DXQUATERNION
	{
	public:
		Quaternion(D3DXQUATERNION &q) : D3DXQUATERNION(q) {}
		Quaternion() : D3DXQUATERNION() {}

		static const Quaternion g_Identity;

	public:
		void Normalize() {};
		void Slerp(const Quaternion &begin, const Quaternion &end, float cooef)
		{
			//performs spherical linear interpolation between begin and end
			//note: set cooef between 0.0f - 1.0f
			D3DXQuaternionSlerp(this, &begin, &end, cooef);
		}

		void GetAxisAngle(Vector3 &axis, float &angle)const
		{
			D3DXQuaternionToAxisAngle(this, &axis, &angle);
		}

		void BuildRotYawPitchRoll(const float yawRadians,
								  const float pitchRadians,
								  const float rollRadians)
		{
			D3DXQuaternionRotationYawPitchRoll(this, yawRadians, pitchRadians, rollRadians);
		}

		void BuildAxisAngle(const Vector3 &axis, const float radians)
		{
			D3DXQuaternionRotationAxis(this, &axis, radians);
		}

		void Build(const Matrix4 &matrix)
		{
			//D3DXQuaternionRotationMatrix(this, &matrix);
		}
	};

	const Quaternion Quaternion::g_Identity(D3DXQUATERNION(0,0,0,1));

	inline Quaternion operator * (const Quaternion &a, const Quaternion &b)
	{
		// for rotaitons, this is exactly like concatenating
		// matrices - the new quat represents rot A follwed by rot B.
		//Quaternion out;
		//D3DXQuaternionMultiply(&out, &a, &b);
		//return out;

		return a * b;
	}




	class Matrix4 : public D3DXMATRIX
	{
	public:
		Matrix4(D3DXMATRIX &matrix)							{ memcpy(&m, &matrix.m, sizeof(matrix.m));			}
		Matrix4() : D3DXMATRIX()							{ }

		static const Matrix4 g_Identity;

	public:
		inline void SetPosition(Vector3 const &pos)
		{
			m[3][0] = pos.x;
			m[3][1] = pos.y;
			m[3][2] = pos.z;
			m[3][3] = 1.0f;
		}

		inline void setPosition(Vector4 const &pos)
		{
			m[3][0] = pos.x;
			m[3][1] = pos.y;
			m[3][2] = pos.z;
			m[3][3] = pos.w;
		}

		inline Vector3 GetPosition() const
		{
			return Vector3(m[3][0], m[3][1], m[3][2]);
		}

		inline Vector4 Xform(Vector4 &vec) const
		{
			Vector4 temp(vec), out;
			D3DXVec4Transform(&temp, &temp, this);
			return Vector3 (out.x, out.y, out.z);
		}

		inline Matrix4 Inverse() const
		{
			Matrix4 out;
			D3DXMatrixInverse(&out, NULL, this);
			return out;
		}

		inline void BuildTranslation(const Vector3 &pos)
		{
			*this = Matrix4::g_Identity;
			m[3][0]  = pos.x; m[3][1] = pos.y; m[3][2] = pos.z;
		}

		inline void BuildTranslation(const float x, const float y, const float z)
		{
			*this = Matrix4::g_Identity;
			m[3][0] = x; m[3][1] = y; m[3][2] = z;
		}

		inline void BuildRotationX(const float radians)					{ D3DXMatrixRotationX(this, radians); }
		inline void BuildRotationY(const float radians)					{ D3DXMatrixRotationY(this, radians); }
		inline void BuildRotationZ(const float radians)					{ D3DXMatrixRotationZ(this, radians); }
		inline void BuildYawPitchROll(const float yawRadians, 
			                          const float pitchRadians, 
									  const float rollRadians)
		{
			D3DXMatrixRotationYawPitchRoll(this, yawRadians, pitchRadians, rollRadians); 
		}
		
		inline void BuildRotationQuat(const Quaternion &q)
		{
			D3DXMatrixRotationQuaternion(this, &q);
		}

		inline void BuildRotationLookAt(const Vector3 &eye, const Vector3 &at, const Vector3 &up)
		{
			D3DXMatrixLookAtRH(this, &eye, &at, &up);
		}


	};

	const Matrix4 Matrix4::g_Identity(D3DXMATRIX(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1));

	inline Matrix4 operator* (const Matrix4 &a, const Matrix4 &b)
	{
		Matrix4 out;
		D3DXMatrixMultiply(&out, &a, &b);
		return out;
	}


}