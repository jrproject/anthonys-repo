#include "LevelFactory.h"
#include "MantisMesh.h"
#include "MemoryManager.h"
#include "Actor.h"

using namespace std;

LevelFactory::LevelFactory(void)
{
	MMI->InitMemory();
}


LevelFactory::~LevelFactory(void)
{
}

// Private Functions
//=====================================
void LevelFactory::ReadSchematics()
{
	//string type; For later use 
	string SchematicName, filename, details[5];

	for(auto schematics : m_JSONObject.m_arrays.at("Schematics").m_values)
	{
		//type = schematics.m_variables["Type"]; For later use
		filename = schematics.m_variables["fileName"];
		SchematicName = schematics.m_variables["Name"];

		auto savedTextures = schematics.m_objects["Textures"];

		details[0] = savedTextures.m_variables["Diffuse"];
		details[1] = savedTextures.m_variables["Bump"];
		details[2] = savedTextures.m_variables["Normal"];
		details[3] = savedTextures.m_variables["Specular"];
		details[4] = savedTextures.m_variables["Illumination"];

		MMI->RegisterNewMeshSchematic(SchematicName, filename, details);
	}
}

void LevelFactory::CreateActors()
{
	string SchematicToUse;
	Actor newActor;
	MantisMesh* tempMesh;

	for(auto actors :  m_JSONObject.m_arrays.at("Actors").m_values)
	{
		SchematicToUse = actors.m_variables["Schematic"];

		tempMesh = MMI->CreateMantisMesh(SchematicToUse);
		tempMesh->m_position = StringToVector3(actors.m_variables["Position"]);
		tempMesh->m_scale = StringToVector3(actors.m_variables["Scale"]);

		//newActor.Graphics = tempMesh;

		
	}
}
//*************************************


// Public Functions
//=====================================
void LevelFactory::BuildLevel(string filename)
{
	JSONParser parser;
	parser.OpenFile(filename);

	m_JSONObject = parser.GetParsedObject();

	if(m_JSONObject.m_arrays.size() == 0) return; //if the file is empty

	ReadSchematics();
	CreateActors();

	MMI->Defragment();
}
//*************************************