#include "WorkBench.h"
#include "WindowsDelegateMethods.h"
#include "WorldManager.h"
#include "Camera.h"
#include "WinMain.h"
#include "MantisMesh.h"
#include "Actor.h"

#include "DXMath.h"

using namespace std;

WorkBench* WorkBench::Instance()
{
	static WorkBench temp;
	return &temp;
}

WorkBench::WorkBench(void)
{
	m_Transforming = false;
	m_DoubleClick = false;
	m_TransformMode = TRANSFORM_TRANSLATE;
	m_anchor = new HelperAnchor();

	DEBUGNOW = false;
}


WorkBench::~WorkBench(void)
{

}

void WorkBench::Unload()
{
	delete m_anchor;
}

void WorkBench::Reset()
{
	m_SelectedMeshes.clear();
	m_Transforming = false;
	m_DoubleClick = false;
	m_TransformMode = TRANSFORM_TRANSLATE;

	delete m_anchor;
	m_anchor = new HelperAnchor();
}

// Inline Utility Functions
//=========================================
inline void Highlight(MantisMesh* mesh)
{
	if(mesh->m_isSelected) return;

	mesh->m_material.ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mesh->m_material.diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mesh->m_material.spec = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	mesh->m_material.specPower = 0.0f;
}

inline void UnHighlight(MantisMesh* mesh)
{
	if(mesh->m_isSelected) return;

	mesh->m_material.ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mesh->m_material.diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	mesh->m_material.spec = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f);
	mesh->m_material.specPower = 50.0f;
}

inline void Select(MantisMesh* mesh)
{
	mesh->m_material.ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mesh->m_material.diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mesh->m_material.spec = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mesh->m_material.specPower = 0.0f;
}

inline void UnSelect(MantisMesh* mesh)
{
	UnHighlight(mesh);	
}

//=========================================

// Private Functions
//=========================================
void WorkBench::GetWorldPickingRay(D3DXVECTOR3 &originW, D3DXVECTOR3 &dirW)
{
	// Get the screen point clicked.
	POINT s = gDInput->mousePosition();

	// Make it relative to the client area window.
	//ScreenToClient(m_owningApp->mhMainWnd, &s);

	// By the way we've been constructing things, the entire 
	// backbuffer is the viewport.

	
	float w = (float)m_owningApp->md3dPP.BackBufferWidth;
	float h = (float)m_owningApp->md3dPP.BackBufferHeight;

	D3DXMATRIX proj = gCamera->proj();

	float x = (2.0f*s.x/w - 1.0f) / proj(0,0);
	float y = (-2.0f*s.y/h + 1.0f) / proj(1,1);

	// Build picking ray in view space.
	D3DXVECTOR3 origin(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 dir(x, y, 1.0f);

	// So if the view matrix transforms coordinates from 
	// world space to view space, then the inverse of the
	// view matrix transforms coordinates from view space
	// to world space.
	D3DXMATRIX invView;
	D3DXMatrixInverse(&invView, 0, &gCamera->view());

	// Transform picking ray to world space.
	D3DXVec3TransformCoord(&originW, &origin, &invView);
	D3DXVec3TransformNormal(&dirW, &dir, &invView);
	D3DXVec3Normalize(&dirW, &dirW);
}

BOOL WorkBench::MouseIsOverlapping(MantisMesh* mesh, D3DXVECTOR3 &originW, D3DXVECTOR3 &dirW)
{
	D3DXMATRIX R, T, S;
	D3DXMatrixTranslation(&T, mesh->m_position.x, mesh->m_position.y, mesh->m_position.z);
	D3DXMatrixScaling(&S, mesh->m_scale.x, mesh->m_scale.y, mesh->m_scale.z);

	D3DXMATRIX toWorld = S * T;

	AABB box;
	mesh->m_AABB->xform(toWorld, box);

	D3DXVECTOR3 transformedPoint;
	D3DXVec3TransformCoord(&transformedPoint, &mesh->m_Sphere->pos, &toWorld);

	if(D3DXSphereBoundProbe(&transformedPoint, mesh->m_Sphere->radius, &originW, &dirW))
		return D3DXBoxBoundProbe(&box.minPt, &box.maxPt, &originW, &dirW);
	else return FALSE;
}

bool WorkBench::ClickedOnMesh(MantisMesh *mesh)
{
	if(gDInput->mouseButtonDown(0))
	{
		if(gDInput->keyDown(DIK_LCONTROL))
		{
			AddToSelection(mesh);
		}
		else if(gDInput->keyDown(DIK_LALT))
		{
			SubtractFromSelection(mesh);
		}
		else
		{
			ClearSelection();
			AddToSelection(mesh);
		}

		return true;
	}

	return false;
}

void WorkBench::RemoveFromSelected(MantisMesh *mesh)
{
	for(std::vector<MantisMesh*>::iterator element = m_SelectedMeshes.begin();
		element != m_SelectedMeshes.end();
		element++)
	{
		if((*element) == mesh)
		{
			m_SelectedMeshes.erase(element);
			return;
		}
	}
}

void WorkBench::ClearSelection()
{
	for(auto mesh : m_SelectedMeshes)
	{
		mesh->m_isSelected = false;
		UnSelect(mesh);
	}

	m_SelectedMeshes.clear();
}

void WorkBench::AddToSelection(MantisMesh *mesh)
{
	if(mesh->m_isSelected) return;

	m_anchor->SetAttachPoint(&mesh->m_position);
	mesh->m_isSelected = true;
	m_SelectedMeshes.push_back(mesh);
	Select(mesh);
}

void WorkBench::SubtractFromSelection(MantisMesh *mesh)
{
	for(std::vector<MantisMesh*>::iterator element = m_SelectedMeshes.begin();
		element != m_SelectedMeshes.end();
		element++)
	{
		if((*element) == mesh)
		{
			m_SelectedMeshes.erase(element);
			mesh->m_isSelected = false;
			UnSelect(mesh);
			return;
		}
	}
}

void WorkBench::Transforms(float dt)
{
	if(m_TransformMode == TRANSFORM_NONE) return;

	WDM->LevelChange();

	switch(m_TransformMode)
	{
		case TRANSFORM_TRANSLATE:
			ApplyTranslation(dt);
			break;

		case TRANSFORM_ROTATE:
			ApplyRotation(dt);
			break;

		case TRANSFORM_SCALE:
			ApplyScale(dt);
			break;
	}
}

void WorkBench::ApplyTranslation(float dt)
{
	D3DXVECTOR3 translation(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 camPos = gCamera->pos();
	float sensativity = gCamera->fixedDist() / 1000;


	switch(m_axis)
	{
		case 1: //x axis
			if(camPos.z < -20.0)
				translation += D3DXVECTOR3((gDInput->mouseDX() * sensativity), 0.0f, 0.0f);
			else if (camPos.z > 20.0)
				translation -= D3DXVECTOR3((gDInput->mouseDX() * sensativity), 0.0f, 0.0f);
			else
				if(camPos.x > 0)
					translation += D3DXVECTOR3((gDInput->mouseDY() * sensativity), 0.0f, 0.0f);
				else
					translation -= D3DXVECTOR3((gDInput->mouseDY() * sensativity), 0.0f, 0.0f);
			break;

		case 2: //y axis

			translation -= D3DXVECTOR3(0.0f, (gDInput->mouseDY() * sensativity), 0.0f);

			break;

		case 3: //z axis
			if(camPos.x < -20.0)
				translation -= D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDX() * sensativity));
			else if (camPos.x > 20.0)
				translation += D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDX() * sensativity));
			else
				if(camPos.z > 0)
					translation += D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDY() * sensativity));
				else
					translation -= D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDY() * sensativity));
			break;

		default:
			return;
	}

	for(auto mesh : m_SelectedMeshes)
	{
		mesh->m_position += translation;
	}
}

void WorkBench::ApplyRotation(float dt)
{
	D3DXVECTOR2 change;
	change.x = (gDInput->mouseDX() * 5.0f) * dt;
	change.y = (gDInput->mouseDY() * 5.0f) * dt;

	D3DXQUATERNION quat;
	D3DXVECTOR3 camPos = gCamera->pos();
	float sensativity = gCamera->fixedDist() / 1000;

	switch(m_axis)
	{
		case 1:
			D3DXQuaternionRotationYawPitchRoll(&quat, 0, change.x, 0);	//around the x axis
			break;

		case 2:
			D3DXQuaternionRotationYawPitchRoll(&quat, change.x, 0, 0);		//around the y axis
			break;

		case 3:
			D3DXQuaternionRotationYawPitchRoll(&quat, 0, 0, change.y);	//around the z axis
			break;
	}
	
	for(auto mesh : m_SelectedMeshes)
	{
		mesh->m_orientation *= quat;
	}
}

void WorkBench::ApplyScale(float dt)
{
	D3DXVECTOR2 change;
	D3DXVECTOR3 camPos = gCamera->pos();
	D3DXVECTOR3 translation(0.0f, 0.0f, 0.0f);
	float sensativity = gCamera->fixedDist() / 1000;

	change.x = (gDInput->mouseDX() * 5.0f) * dt;
	change.y = (gDInput->mouseDY() * 5.0f) * dt;

	switch(m_axis)
	{
		case 1: //x axis
			if(camPos.z < -20.0)
				translation += D3DXVECTOR3((gDInput->mouseDX() * sensativity), 0.0f, 0.0f);
			else if (camPos.z > 20.0)
				translation -= D3DXVECTOR3((gDInput->mouseDX() * sensativity), 0.0f, 0.0f);
			else
				if(camPos.x > 0)
					translation += D3DXVECTOR3((gDInput->mouseDY() * sensativity), 0.0f, 0.0f);
				else
					translation -= D3DXVECTOR3((gDInput->mouseDY() * sensativity), 0.0f, 0.0f);
			break;

		case 2: //y axis

			translation -= D3DXVECTOR3(0.0f, (gDInput->mouseDY() * sensativity), 0.0f);

			break;

		case 3: //z axis
			if(camPos.x < -20.0)
				translation -= D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDX() * sensativity));
			else if (camPos.x > 20.0)
				translation += D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDX() * sensativity));
			else
				if(camPos.z > 0)
					translation += D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDY() * sensativity));
				else
					translation -= D3DXVECTOR3(0.0f, 0.0f, (gDInput->mouseDY() * sensativity));
			break;

		default:
			return;
	}

	for(auto mesh : m_SelectedMeshes)
	{
		mesh->m_scale += translation;

		if(mesh->m_scale.x <= 0.0f)
			mesh->m_scale.x = 0.0f;

		if(mesh->m_scale.y <= 0.0f)
			mesh->m_scale.y = 0.0f;

		if(mesh->m_scale.z <= 0.0f)
			mesh->m_scale.z = 0.0f;
	}
}

bool WorkBench::AnchorInteraction()
{
	//this function will check to see if the user
	//is interacting with the anchor mesh
	//if there is an interaction then the user is
	//performing a transformation
	if(m_SelectedMeshes.size() == 0)
		return false;

	if(!gDInput->mouseButtonDown(0))
	{
		m_Transforming = false;
		return false;
	}

	if(!m_Transforming)
	{
		D3DXVECTOR3 originW(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 dirW(0.0f, 0.0f, 0.0f);

		GetWorldPickingRay(originW, dirW);
		m_axis = m_anchor->ClickedOn(originW, dirW);

		if (m_axis == 0)
		{
			m_Transforming = false;
			return false;
		}
	}

	m_Transforming = true;

	return true;
}

void WorkBench::CheckCameraSnap()
{
	if(gDInput->keyPressed(DIK_C))
		if(m_SelectedMeshes.size() != 0)
			gCamera->SnapToLookAtPoint(m_SelectedMeshes.front()->m_position);

}

void WorkBench::NewSelectedActor()
{
	if(m_SelectedMeshes.size() > 1)
	{
		m_SelectedActor = nullptr;
	}
	else
	{
		m_SelectedActor = MMI->getActor(m_SelectedMeshes.front()->m_owningActorID);
	}

	cout << "Selected Actor: " << m_SelectedActor->ID << endl;

	WDM->UpdateProperties();
}

void WorkBench::SetSelectedActor(Actor* newActor)
{
	m_SelectedActor = newActor;

	m_SelectedMeshes.clear();

	m_SelectedMeshes.push_back(newActor->Graphics);

	WDM->UpdateProperties();
}

//=========================================

// public Functions
//=========================================
void WorkBench::SetApplication(Game *game)
{
	m_owningApp = game;
}

void WorkBench::Update(float dt)
{
	if(gDInput->keyPressed(DIK_SPACE))
		NextTransformMode();

	auto meshObjects = WMI->getDrawableMeshes();

	DEBUGNOW = false;

	D3DXVECTOR3 originW(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 dirW(0.0f, 0.0f, 0.0f);

	GetWorldPickingRay(originW, dirW);

	bool interaction = false;
	//bool TouchedObject = false;

	CheckCameraSnap();

	if(!AnchorInteraction() && !m_Transforming)
	{
		for(auto type : meshObjects)
		{
			for(auto objectOfType : *type)
			{
				if(MouseIsOverlapping(objectOfType, originW, dirW))
				{
					Highlight(objectOfType);

					if(ClickedOnMesh(objectOfType))
					{
						interaction = true;
						NewSelectedActor();
						//break;
						continue;
					}

					//TouchedObject = true;
					//break;

				} else {
					UnHighlight(objectOfType);
				}
			}

			//if(TouchedObject)
				//break;
		}

		if(!interaction)
		{
			if(gDInput->mouseButtonDown(0))
				ClearSelection();
		}
	}
	else
	{
		Transforms(dt);
	}
		
	m_anchor->Update();
}

void WorkBench::SetTransformMode(int mode)
{
	m_TransformMode = mode;
}

void WorkBench::NextTransformMode()
{
	m_TransformMode++;

	if(m_TransformMode == TRANSFORM_END)
		m_TransformMode = TRANSFORM_TRANSLATE;

	//UpdateAnchor();
}

Actor* WorkBench::getSelectedActor()
{
	return m_SelectedActor;
}
//=========================================