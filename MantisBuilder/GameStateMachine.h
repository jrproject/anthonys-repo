#pragma once
#include "GameState.h"


class GameStateMachine{
private:

	GameState* m_CurrentState;
	GameState* m_PreviousState;
	GameState* m_GlobalState;

public:
	GameStateMachine():m_CurrentState(0),
					   m_PreviousState(0),
					   m_GlobalState(0) {}

	virtual ~GameStateMachine()
	{
		delete m_PreviousState;
		delete m_CurrentState;
	}

	void SetCurrentState(GameState* gs)
	{
		m_CurrentState = gs;
	}

	void SetGlobalState(GameState* gs)
	{
		m_GlobalState = gs;
	}

	void SetPreviousState(GameState* gs)
	{
		m_PreviousState = gs;
	}

	void InitializeState()const
	{
		m_CurrentState->InitializeState();
	}

	void UpdateScene(float dt)const
	{
		m_CurrentState->UpdateScene(dt);
	}

	void RenderScene()const
	{
		m_CurrentState->RenderScene();
	}

	void OnResetDevice()const
	{
		m_CurrentState->OnResetDevice();
	}

	void OnLostDevice()const
	{
		m_CurrentState->OnLostDevice();
	}

	void LeaveState()const
	{
		m_CurrentState->LeaveState();
	}

	//messages

	void ChangeState(GameState* newState)
	{
		delete m_PreviousState;

		m_PreviousState = m_CurrentState;
		m_CurrentState->LeaveState();
		m_CurrentState = newState;
		m_CurrentState->InitializeState();
	}

	void ReverToPreviousState()
	{
		//m_PreviousState = m_CurrentState;
		m_CurrentState->LeaveState();

		GameState* temp = m_CurrentState;

		m_CurrentState = m_PreviousState;
		m_PreviousState = temp;

		m_CurrentState->InitializeState();
	}
};

