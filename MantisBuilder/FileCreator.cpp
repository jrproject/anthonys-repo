#include "FileCreator.h"
#include <Windows.h>
#include <stack>

#include "WindowsDelegateMethods.h" //for progress bar

using namespace std;

FileCreator::FileCreator()
{
}


FileCreator::~FileCreator()
{
}

// private functions
//==============================
int FileCreator::CountFiles(string path)
{
	int count = 0;

	HANDLE hFind = INVALID_HANDLE_VALUE;
    WIN32_FIND_DATA ffd;
    string spec;
    stack<string> directories;

    directories.push(path);

    while (!directories.empty()) {
        path = directories.top();
        spec = path + "\\*";
        directories.pop();

        hFind = FindFirstFile(spec.c_str(), &ffd);
        if (hFind == INVALID_HANDLE_VALUE)  {
            return -1;
        } 

        do {
            if((string(ffd.cFileName) != ".") && (string(ffd.cFileName) != "..")){
                if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                    directories.push(path + "\\" + ffd.cFileName);
                }
                else {
					count++;
                }
            }
        } while (FindNextFile(hFind, &ffd) != 0);

        if (GetLastError() != ERROR_NO_MORE_FILES) {
            FindClose(hFind);
            return false;
        }

        FindClose(hFind);
        hFind = INVALID_HANDLE_VALUE;
    }

	return count;
}

// public functions
//==============================
void FileCreator::CopyDirectory(string path, string DirectoryToCopyTo)
{
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;

	string mask = "*";
	string spec = path + "\\" + mask;

	hFind = FindFirstFile(spec.c_str(), &ffd);
	if (hFind == INVALID_HANDLE_VALUE) return;

	do {
		if((string(ffd.cFileName) != ".") && (string(ffd.cFileName) != "..")){
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				CreateDirectory((DirectoryToCopyTo + "\\" + ffd.cFileName).c_str(), NULL);
				CopyDirectory(path + "\\" + ffd.cFileName, DirectoryToCopyTo + "\\" + ffd.cFileName);
			}
			else {
				CopyFile((path + "\\" + ffd.cFileName).c_str(), (DirectoryToCopyTo + "\\" + ffd.cFileName).c_str(), FALSE);
				WDM->ProgressBarUpdate(ffd.cFileName);
			}
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	if (GetLastError() != ERROR_NO_MORE_FILES) {
		FindClose(hFind);
		return;
	}

	FindClose(hFind);
	hFind = INVALID_HANDLE_VALUE;
}

void FileCreator::HandleFiles(vector<string> Files, string directory)
{
	string fileName;

	for(string File : Files)
	{
		if(File.back() == '\0')
			File.pop_back();

		for(auto it = File.rbegin();
			it != File.rend();
			it++)
		{	
			if(*it == '\\')
			{
				it++;
				fileName = string(it.base(), File.end());
				break;
			}
		}

		DWORD result = GetFileAttributes(File.c_str());

		if(result == FILE_ATTRIBUTE_DIRECTORY)
		{
			int fileCount = CountFiles(File);
			WDM->ProgressBar(fileCount, "Copying: ");

			CreateDirectory((directory + fileName).c_str(), NULL);
			CopyDirectory(File, (directory + fileName)); 

			WDM->ProgressBarShutdown();

			char buffer[100];

			WDM->MessageAlert("Completed Transfer",
							  "Successfully transfered " + string(itoa(fileCount, buffer, 10)) + " Files.",
							  MB_OK | MB_ICONINFORMATION,
							  MB_ICONINFORMATION,
							  true);
		}
		else
		{
			CopyFile(File.c_str(), (directory + fileName).c_str(), FALSE);
		}
	}
}