#pragma once
#include "d3dUtil.h"

class ZonesDialog
{
private:

	HWND dialogHWND;

public:
	ZonesDialog(void);
	~ZonesDialog(void);

	BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

