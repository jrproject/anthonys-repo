#include "MemoryManager.h"
#include "Actor.h"
#include "Vertex.h"
#include <cassert>

#include "WorkBench.h"


MemoryManager* MemoryManager::Instance()
{
	static MemoryManager temp;
	return &temp;
}

MemoryManager::MemoryManager()
{
	m_memoryInitalized = false;
}

MemoryManager::~MemoryManager()
{
}

//memory meta functions
//======================================
void MemoryManager::InitMemory()
{
	//if the memory is initalized don't bother
	//otherwise allocate everything
	if(m_memoryInitalized) return;

	//initialize anything that needs to be done once
	InitAllVertexDeclarations();

	m_memoryInitalized = true;
}

void MemoryManager::CleanUp()
{
	//if the memory is not initialized don't bother
	//otherwise deallocate everything
	if(!m_memoryInitalized) return;

	DestroyAllVertexDeclarations();

	//Release Components
	for(auto schematic : m_MeshSchematics)
	{
		//ReleaseCOM(schematic.second.mesh);
		delete schematic.second.BoundingBox;
		delete schematic.second.BoundingSphere;
	}

	for(auto mesh : m_Meshes)
	{
		ReleaseCOM(mesh.second);
	}

	for(auto texture : m_Textures)
	{
		ReleaseCOM(texture.second);
	}

	m_MeshSchematics.clear();
	m_Meshes.clear();
	m_Textures.clear();

	//delete allocated memory
	//for(auto map : m_GraphicObjects)
	//	for(auto mesh : map.second)
	//		delete mesh;

	for(auto actor : m_Actors)
		delete actor.second;

	m_GraphicObjects.clear();
	m_AllGraphicObjects.clear();
	m_Actors.clear();

	m_memoryInitalized = false;

	// EDITOR MEMORY

	for(auto texture : m_EDITOR_Textures)
	{
		ReleaseCOM(texture.second);
	}

	for(auto schematic : m_EDITOR_MeshSchematics)
	{
		ReleaseCOM(schematic.second.mesh);
		ReleaseCOM(schematic.second.maps[0]);

		delete schematic.second.BoundingBox;
		delete schematic.second.BoundingSphere;
	}

	for(auto map : m_EDITOR_GraphicObjects)
		for(auto mesh : map.second)
			delete mesh; 

	m_EDITOR_MeshSchematics.clear();
	m_EDITOR_GraphicObjects.clear();
	m_EDITOR_AllGraphicObjects.clear();

	cout << "Memory Performed CleanUp" << endl;
}

void MemoryManager::IsMemoryGood()
{
	assert(m_memoryInitalized && "MEMORY IS NOT INITALIZED");
}

void MemoryManager::ReloadMemory()
{
	MMI->CleanUp();
	MMI->InitMemory();
}

void MemoryManager::Defragment()
{
	//check for empty schematic vectors
	for(auto schematicVector = m_GraphicObjects.begin();
		schematicVector != m_GraphicObjects.end();
		schematicVector++)
	{
		if(schematicVector->second.size() == 0)
			m_GraphicObjects.erase(schematicVector);
	}

	//rebuild iteration vector for meshes
	m_AllGraphicObjects.clear();
	for(auto schematicVector = m_GraphicObjects.begin();
		schematicVector != m_GraphicObjects.end();
		schematicVector++)
	{
		m_AllGraphicObjects.push_back(&schematicVector->second);
	}

	cout << "Memory Performed Defrag" << endl;
}
//**************************************

// Utility Functions
ID3DXMesh* MemoryManager::CalculateTBN(ID3DXMesh* mesh)
{
	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	UINT numElements = 0;
	VertexNormalMap::Decl->GetDeclaration(elems, &numElements);

	ID3DXMesh* cloneTempMesh = 0;
	mesh->CloneMesh(D3DXMESH_MANAGED, elems, gd3dDevice, &cloneTempMesh);

	ReleaseCOM(mesh);
	mesh = 0;

	D3DXComputeTangentFrameEx(
			cloneTempMesh,			  // Input mesh
			D3DDECLUSAGE_TEXCOORD, 0, // Vertex element of input tex-coords.  
			D3DDECLUSAGE_BINORMAL, 0, // Vertex element to output binormal.
			D3DDECLUSAGE_TANGENT, 0,  // Vertex element to output tangent.
			D3DDECLUSAGE_NORMAL, 0,   // Vertex element to output normal.
			0,						  // Options
			0,						  // Adjacency
			0.01f, 0.25f, 0.01f,	  // Thresholds for handling errors
			&mesh,				  // Output mesh
			0);

	ReleaseCOM(cloneTempMesh);

	return mesh;
}
//======================================

// Resource Allocation
//======================================
LPDIRECT3DTEXTURE9 MemoryManager::AllocateNewTexture(string resourceName, string resourcePath)
{
	if (resourceName == "NULL") return nullptr;

	IsMemoryGood();

	LPDIRECT3DTEXTURE9 returnTexture = NULL;

	auto result = m_Textures.find(resourceName);

	if(result == m_Textures.end())
	{
		D3DXCreateTextureFromFile(gd3dDevice, resourcePath.c_str(), &returnTexture);

		//assert((returnTexture != NULL) && "TEXTURE NOT FOUND");

		if(returnTexture == nullptr)
			return NULL;

		cout << "Allocated new Texture: " << endl;
		cout << "\tName: " << resourceName << endl;
		cout << "\tPath: " << resourcePath << endl << endl;

		m_Textures.insert(make_pair(resourceName, returnTexture));

		return returnTexture;
	}
	else
	{
		return result->second;
	}
}

ID3DXMesh* MemoryManager::AllocateNewMesh(string resourceName, string resourcePath)
{
	IsMemoryGood();

	ID3DXMesh* returnMesh;

	auto result = m_Meshes.find(resourceName);

	if(result == m_Meshes.end())
	{
		D3DXLoadMeshFromX(resourcePath.c_str(), D3DXMESH_SYSTEMMEM, gd3dDevice, NULL, NULL, NULL, NULL, &returnMesh);

		cout << "Allocated new Mesh: " << endl;
		cout << "\tName: " << resourceName << endl;
		cout << "\tPath: " << resourcePath << endl << endl;

		assert((returnMesh != NULL) && "MESH NOT FOUND");

		returnMesh = CalculateTBN(returnMesh);

		m_Meshes.insert(make_pair(resourceName, returnMesh));

		return returnMesh;
	}
	else
	{
		return result->second;
	}
}
//**************************************

// Schematic Management
//======================================
void MemoryManager::RegisterNewMeshSchematic(string SchematicName, string filename, string details[5])
{
	IsMemoryGood();

	MeshSchematic schematic;
	DWORD detailFlags = 0;

	schematic.fileName	= filename;
	schematic.name		= SchematicName;
	
	//1 construct schematic since it does not exists
	ID3DXMesh* tempMesh = AllocateNewMesh(filename, "Assets\\Meshes\\" + filename + "\\" + filename +".X");
	  	
	schematic.mesh = tempMesh;

	//Calculate Bounding volumes
	//==================================
	VertexNormalMap *pVerts = 0;

	BoundingSphere *Sphere	= new BoundingSphere();
	AABB		   *box		= new AABB();

	schematic.mesh->LockVertexBuffer(0, (void**)&pVerts);

	D3DXComputeBoundingSphere(&pVerts[0].pos,
								schematic.mesh->GetNumVertices(),
								sizeof(VertexNormalMap),
								&Sphere->pos,
								&Sphere->radius);

	D3DXComputeBoundingBox(&pVerts[0].pos,
							schematic.mesh->GetNumVertices(),
							sizeof(VertexNormalMap),
							&box->minPt,
							&box->maxPt);

	schematic.mesh->UnlockVertexBuffer();

	schematic.BoundingBox = box;
	schematic.BoundingSphere = Sphere;
	//**********************************

	// Assign Detail Maps
	//==================================
	if(details[DIFFUSE_MAP] != "")
	{
		schematic.maps[DIFFUSE_MAP] = AllocateNewTexture(details[DIFFUSE_MAP], "Assets\\Meshes\\" + filename + "\\Diffuse\\" + details[DIFFUSE_MAP]);
		schematic.mapNames[DIFFUSE_MAP] = details[DIFFUSE_MAP];
		detailFlags |= DIF;
	}

	if(details[BUMP_MAP] != "")
	{
		schematic.maps[BUMP_MAP] = AllocateNewTexture(details[BUMP_MAP], "Assets\\Meshes\\" + filename + "\\Bump\\" + details[BUMP_MAP]);
		schematic.mapNames[BUMP_MAP] = details[BUMP_MAP];
		detailFlags |= BMP;
	}

	if(details[NORMAL_MAP] != "")
	{
		schematic.maps[NORMAL_MAP] = AllocateNewTexture(details[NORMAL_MAP], "Assets\\Meshes\\" + filename + "\\Normal\\" + details[NORMAL_MAP]);
		schematic.mapNames[NORMAL_MAP] = details[NORMAL_MAP];
		detailFlags |= NRM;
	}

	if(details[SPECULAR_MAP] != "")
	{
		schematic.maps[SPECULAR_MAP] = AllocateNewTexture(details[SPECULAR_MAP], "Assets\\Meshes\\" + filename + "\\Specular\\" + details[SPECULAR_MAP]);
		schematic.mapNames[SPECULAR_MAP] = details[SPECULAR_MAP];
		detailFlags |= SPC;
	}

	if(details[ILLUMINATION_MAP] != "")
	{
		schematic.maps[ILLUMINATION_MAP] = AllocateNewTexture(details[ILLUMINATION_MAP], "Assets\\Meshes\\" + filename + "\\Illumination\\" + details[ILLUMINATION_MAP]);
		schematic.mapNames[ILLUMINATION_MAP] = details[ILLUMINATION_MAP];
		detailFlags |= ILL;
	}
	//**********************************

	//assign detail map flag
	schematic.detailFlags = detailFlags;

	//verify for duplicates and add to schematic map
	DoesExist(schematic);

	m_MeshSchematics.insert(make_pair(schematic.name, schematic));
}

MeshSchematic MemoryManager::RegisterNewMeshSchematic(string SchematicName, string filename, string AbsolutePath)
{
	IsMemoryGood();

	MeshSchematic schematic;
	DWORD detailFlags = 0;

	schematic.fileName = filename;
	schematic.name = SchematicName;

	//1 construct schematic since it does not exists
	ID3DXMesh* tempMesh = AllocateNewMesh(filename, AbsolutePath + "Assets\\Meshes\\" + filename + "\\" + filename +".X");

	schematic.mesh = tempMesh;
		
	//Calculate Bounding volumes
	VertexNormalMap *pVerts = 0;

	BoundingSphere *Sphere	= new BoundingSphere();
	AABB		   *box		= new AABB();

	schematic.mesh->LockVertexBuffer(0, (void**)&pVerts);

	D3DXComputeBoundingSphere(&pVerts[0].pos,
								schematic.mesh->GetNumVertices(),
								sizeof(VertexNormalMap),
								&Sphere->pos,
								&Sphere->radius);

	D3DXComputeBoundingBox(&pVerts[0].pos,
							schematic.mesh->GetNumVertices(),
							sizeof(VertexNormalMap),
							&box->minPt,
							&box->maxPt);

	schematic.mesh->UnlockVertexBuffer();

	schematic.BoundingBox = box;
	schematic.BoundingSphere = Sphere;

	//detect detail maps

	schematic.maps[DIFFUSE_MAP] = AllocateNewTexture(filename + "_DIF.jpg", AbsolutePath + "Assets\\Meshes\\" + filename + "\\Diffuse\\" + filename + "_DIF.jpg");
	
	if(schematic.maps[DIFFUSE_MAP] != NULL)
	{
		schematic.mapNames[DIFFUSE_MAP] = filename + "_DIF.jpg";
		detailFlags |= DIF;
	}
	else
	{
		schematic.mapNames[DIFFUSE_MAP] = "NULL";
	}

	schematic.maps[BUMP_MAP] = AllocateNewTexture(filename + "_BMP.jpg", AbsolutePath + "Assets\\Meshes\\" + filename + "\\Bump\\" + filename + "_BMP.jpg");
	if(schematic.maps[BUMP_MAP] != NULL)
	{
		schematic.mapNames[BUMP_MAP] = filename + "_BMP.jpg";
		detailFlags |= BMP;
	}
	else
	{
		schematic.mapNames[BUMP_MAP] = "NULL";
	}

	schematic.maps[NORMAL_MAP] = AllocateNewTexture(filename + "_NRM.jpg", AbsolutePath + "Assets\\Meshes\\" + filename + "\\Normal\\" + filename + "_NRM.jpg");
	if(schematic.maps[NORMAL_MAP] != NULL)
	{
		schematic.mapNames[NORMAL_MAP] = filename + "_NRM.jpg";
		detailFlags |= NRM;
	}
	else
	{
		schematic.mapNames[NORMAL_MAP] = "NULL";
	}

	schematic.maps[SPECULAR_MAP] = AllocateNewTexture(filename + "_SPEC.jpg", AbsolutePath + "Assets\\Meshes\\" + filename + "\\Specular\\" + filename + "_SPEC.jpg");
	if(schematic.maps[SPECULAR_MAP] != NULL)
	{
		schematic.mapNames[SPECULAR_MAP] = filename + "_SPEC.jpg";
		detailFlags |= SPC;
	}
	else
	{
		schematic.mapNames[SPECULAR_MAP] = "NULL";
	}

	schematic.maps[ILLUMINATION_MAP] = AllocateNewTexture(filename + "_ILL.jpg", AbsolutePath + "Assets\\Meshes\\" + filename + "\\Illumination\\" + filename + "_ILL.jpg");
	if(schematic.maps[ILLUMINATION_MAP] != NULL)
	{
		schematic.mapNames[ILLUMINATION_MAP] = filename + "_ILL.jpg";
		detailFlags |= ILL;
	}
	else
	{
		schematic.mapNames[ILLUMINATION_MAP] = "NULL";
	}

	//assign detail map flag
	schematic.detailFlags = detailFlags;

	//verify for duplicates and add to schematic map
	DoesExist(schematic);

	m_MeshSchematics.insert(make_pair(schematic.name, schematic));

	WBI->DEBUGNOW = true;

	//return the newly allocated scehematic
	return schematic;
}

void MemoryManager::RegisterNewMeshSchematic(MeshSchematic Schematic)
{
	auto result = m_MeshSchematics.find(Schematic.name);

	if(result == m_MeshSchematics.end())
	{
		DoesExist(Schematic);
		m_MeshSchematics.insert(make_pair(Schematic.name, Schematic));
	}
}

MeshSchematic MemoryManager::GetMeshSchematic(string SchematicName)
{
	return m_MeshSchematics.at(SchematicName);
}

bool MemoryManager::ContentsDoMatch(MeshSchematic Incoming, MeshSchematic Orginal)
{
	//compare for same mesh
	if(Incoming.mesh != Orginal.mesh)		return false;

	//compre map details
	if(Incoming.maps[0] != Orginal.maps[0]) return false;
	if(Incoming.maps[1] != Orginal.maps[1])	return false;
	if(Incoming.maps[2] != Orginal.maps[2])	return false;
	if(Incoming.maps[3] != Orginal.maps[3]) return false;
	if(Incoming.maps[4] != Orginal.maps[4]) return false;

	return true;
}

void MemoryManager::DoesExist(MeshSchematic &Incoming)
{
	//unique identifer
	static int unique = 0;

	//Check if a schematic by that name exists
	auto result = m_MeshSchematics.find(Incoming.name);

	//if it does not then check all other schematics for content duplicates
	//if duplicate is found then return the original schematic and ignore the new one
	if(result == m_MeshSchematics.end())
	{
		for(auto original : m_MeshSchematics)
		{
			if(ContentsDoMatch(Incoming, original.second))
			{
				Incoming = original.second;
				return;
			}
		}

		return;
	}

	//if a duplicate name is found check for duplicate contents
	//if contents MATCH then return original schematic ignore the new one
	//if contents DO NOT MATCH generate new unique name and check for duplicates against other schematics
	if(ContentsDoMatch(Incoming, result->second))
	{
		Incoming = result->second;
		return;
	}
	else
	{
		char buffer[10];
		Incoming.name = Incoming.name + "_" + itoa(unique, buffer, 10);
		unique++;

		for(auto original : m_MeshSchematics)
		{
			if(ContentsDoMatch(Incoming, original.second))
			{
				Incoming = original.second;
				return;
			}
		}

		return;
	}
}
//**************************************

//Creation Functions
//======================================
MantisMesh* MemoryManager::CreateMantisMesh(string SchematicName)
{
	vector<MantisMesh*> memoryArray;

	Actor *temp = new Actor();

	//pair<map<string, vector<MantisMesh*>>::iterator, bool> ret;
	auto ret = m_GraphicObjects.insert(make_pair(SchematicName, memoryArray));
	if(ret.second == false)
	{
		ret.first->second.push_back(new MantisMesh(m_MeshSchematics[SchematicName]));
	}
	else
	{
		ret.first->second.push_back(new MantisMesh(m_MeshSchematics[SchematicName]));
		m_AllGraphicObjects.push_back(&ret.first->second);

		ret.first->second.back()->m_owningActorID = temp->ID;
	}

	//Give the mesh the ID to the owning actor for reverse lookup
	ret.first->second.back()->m_owningActorID = temp->ID; 

	//Assign the mantis mesh to the actor
	temp->Graphics = ret.first->second.back();

	//insert the actor into the actor map
	m_Actors.insert(make_pair(temp->ID, temp));

	return ret.first->second.back();
}

MantisMesh* MemoryManager::CreateMantisMesh(MeshSchematic Schematic)
{
	vector<MantisMesh*> memoryArray;

	Actor *temp = new Actor();

	//pair<map<string, vector<MantisMesh*>>::iterator, bool> ret;
	auto ret = m_GraphicObjects.insert(make_pair(Schematic.name, memoryArray));
	if(ret.second == false)
	{
		ret.first->second.push_back(new MantisMesh(Schematic));
	}
	else
	{
		ret.first->second.push_back(new MantisMesh(Schematic));
		m_AllGraphicObjects.push_back(&ret.first->second);

		ret.first->second.back()->m_owningActorID = temp->ID;
	}

	//Give the mesh the ID to the owning actor for reverse lookup
	ret.first->second.back()->m_owningActorID = temp->ID; 

	//Assign the mantis mesh to the actor
	temp->Graphics = ret.first->second.back();

	//insert the actor into the actor map
	m_Actors.insert(make_pair(temp->ID, temp));

	return ret.first->second.back();
}

MantisAI* MemoryManager::CreateMantisAI(string AISchematic)
{
	return nullptr;
}

MantisPhysics* MemoryManager::CreateMantisPhysics(string PhysicsSchematic)
{
	return nullptr;
}

void MemoryManager::AddActor(Actor* newActor)
{
	m_Actors.insert(make_pair(newActor->ID, newActor));
}
//**************************************

// Delete Functions
//======================================
void MemoryManager::DeleteMantisMesh(MantisMesh* mesh)
{
	auto meshVector = m_GraphicObjects.find(mesh->m_schematicName);

	for(auto meshIT = meshVector->second.begin();
		meshIT != meshVector->second.end();
		meshIT++)
	{
		if(*meshIT == mesh)
		{
			meshVector->second.erase(meshIT);

			if(meshVector->second.size() == 0)
				m_GraphicObjects.erase(meshVector);

			return;
		}
	}
}
//**************************************

//Accessor functions
//======================================
vector<vector<MantisMesh*>*> MemoryManager::GetGraphicsObjects()
{
	return m_AllGraphicObjects;
}

vector<vector<MantisAI*>*> MemoryManager::GetAIObjects()
{
	return m_AllAIObjects;
}

vector<vector<MantisPhysics*>*> MemoryManager::GetPhysicsObjects()
{
	return m_AllPhysicsObjects;
}

LPDIRECT3DTEXTURE9 MemoryManager::GetTexture(string textureName)
{
	return m_Textures.at(textureName);
}

ID3DXMesh* MemoryManager::GetMesh(string meshName)
{
	return m_Meshes.at(meshName);
}

Actor* MemoryManager::getActor(int id)
{
	return m_Actors.at(id);
}
//**************************************



//Level Editor Helper functions ONLY
//Jesus Christ Merie they're minerals
//======================================
map<string, MeshSchematic> MemoryManager::getSchematics()
{
	return m_MeshSchematics;
}

map<string, vector<MantisMesh*>> MemoryManager::getGraphicsObjects()
{
	return m_GraphicObjects;
}

vector<vector<MantisMesh*>*> MemoryManager::getEditorMeshes()
{
	return m_EDITOR_AllGraphicObjects;
}

LPDIRECT3DTEXTURE9 MemoryManager::AllocateNewEditorTexture(string resourceName, string resourcePath)
{
	IsMemoryGood();

	LPDIRECT3DTEXTURE9 returnTexture = NULL;

	auto result = m_EDITOR_Textures.find(resourceName);

	if(result == m_EDITOR_Textures.end())
	{
		D3DXCreateTextureFromFile(gd3dDevice, resourcePath.c_str(), &returnTexture);

		//assert((returnTexture != NULL) && "TEXTURE NOT FOUND");

		if(returnTexture == nullptr)
			return NULL;

		//m_EDITOR_Textures.insert(make_pair(resourceName, returnTexture));

		return returnTexture;
	}
	else
	{
		return result->second;
	}
}

void MemoryManager::RegisterNewEditorMeshSchematic(string filename)
{
	IsMemoryGood();

	MeshSchematic schematic;
	DWORD detailFlags = 0;

	pair<map<string,MeshSchematic>::iterator,bool> ret;
	ret = m_EDITOR_MeshSchematics.insert(make_pair(filename, schematic));
	if(ret.second == true)
	{		
		//1 construct schematic since it does not exists
		ID3DXMesh* tempMesh = 0;

		D3DXLoadMeshFromX(("Assets\\Meshes\\" + filename + "\\" + filename +".X").c_str(), D3DXMESH_SYSTEMMEM, gd3dDevice, NULL, NULL, NULL, NULL, &tempMesh);
		
	  
		assert((tempMesh != NULL) && "MESH NOT FOUND");
		
		D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
		UINT numElements = 0;
		VertexNormalMap::Decl->GetDeclaration(elems, &numElements);

		ID3DXMesh* cloneTempMesh = 0;
		tempMesh->CloneMesh(D3DXMESH_MANAGED, elems, gd3dDevice, &cloneTempMesh);

		ReleaseCOM(tempMesh);
		tempMesh = 0;

		D3DXComputeTangentFrameEx(
			cloneTempMesh,			  // Input mesh
			D3DDECLUSAGE_TEXCOORD, 0, // Vertex element of input tex-coords.  
			D3DDECLUSAGE_BINORMAL, 0, // Vertex element to output binormal.
			D3DDECLUSAGE_TANGENT, 0,  // Vertex element to output tangent.
			D3DDECLUSAGE_NORMAL, 0,   // Vertex element to output normal.
			0,						  // Options
			0,						  // Adjacency
			0.01f, 0.25f, 0.01f,	  // Thresholds for handling errors
			&tempMesh,				  // Output mesh
			0);

		ret.first->second.mesh = tempMesh;

		ReleaseCOM(cloneTempMesh);
		
		//Calculate Bounding volumes
		VertexNormalMap *pVerts = 0;

		BoundingSphere *Sphere	= new BoundingSphere();
		AABB		   *box		= new AABB();

		ret.first->second.mesh->LockVertexBuffer(0, (void**)&pVerts);

		D3DXComputeBoundingSphere(&pVerts[0].pos,
								  ret.first->second.mesh->GetNumVertices(),
								  sizeof(VertexNormalMap),
								  &Sphere->pos,
								  &Sphere->radius);

		D3DXComputeBoundingBox(&pVerts[0].pos,
							   ret.first->second.mesh->GetNumVertices(),
							   sizeof(VertexNormalMap),
							   &box->minPt,
							   &box->maxPt);

		ret.first->second.mesh->UnlockVertexBuffer();

		ret.first->second.BoundingBox = box;
		ret.first->second.BoundingSphere = Sphere;

		//detect detail maps

		ret.first->second.maps[DIFFUSE_MAP] = AllocateNewEditorTexture(filename + "_DIF.jpg", "Assets\\Meshes\\" + filename + "\\Diffuse\\" + filename + "_DIF.jpg");
		if(ret.first->second.maps[DIFFUSE_MAP] != NULL)
			detailFlags |= DIF;

		ret.first->second.maps[BUMP_MAP] = AllocateNewEditorTexture(filename + "_BMP.jpg", "Assets\\Meshes\\" + filename + "\\Bump\\" + filename + "_BMP.jpg");
		if(ret.first->second.maps[BUMP_MAP] != NULL)
			detailFlags |= BMP;

		ret.first->second.maps[NORMAL_MAP] = AllocateNewEditorTexture(filename + "_NRM.jpg", "Assets\\Meshes\\" + filename + "\\Normal\\" + filename + "_NRM.jpg");
		if(ret.first->second.maps[NORMAL_MAP] != NULL)
			detailFlags |= NRM;

		ret.first->second.maps[SPECULAR_MAP] = AllocateNewEditorTexture(filename + "_SPEC.jpg", "Assets\\Meshes\\" + filename + "\\Specular\\" + filename + "_SPEC.jpg");
		if(ret.first->second.maps[SPECULAR_MAP] != NULL)
			detailFlags |= SPC;

		ret.first->second.maps[ILLUMINATION_MAP] = AllocateNewEditorTexture(filename + "_ILL.jpg", "Assets\\Meshes\\" + filename + "\\Illumination\\" + filename + "_ILL.jpg");
		if(ret.first->second.maps[ILLUMINATION_MAP] != NULL)
			detailFlags |= ILL;

		//assign detail map flag
		ret.first->second.detailFlags = detailFlags;
	}
}

MantisMesh* MemoryManager::CreateEditorMeshInstance(string SchematicName)
{
	vector<MantisMesh*> memoryArray;
	//pair<map<string, vector<MantisMesh*>>::iterator, bool> ret;
	auto ret = m_EDITOR_GraphicObjects.insert(make_pair(SchematicName, memoryArray));
	if(ret.second == false)
	{
		ret.first->second.push_back(new MantisMesh(m_EDITOR_MeshSchematics[SchematicName]));
	}
	else
	{
		ret.first->second.push_back(new MantisMesh(m_EDITOR_MeshSchematics[SchematicName]));
		m_EDITOR_AllGraphicObjects.push_back(&ret.first->second);
	}

	return ret.first->second.back();
}

void MemoryManager::SwapTexture(MantisMesh* GraphicObject, string TextureName, int DetailMap, string AbsolutePath)
{
	auto result = m_Textures.find(TextureName);

	string schematicName = GraphicObject->m_fileName;

	MeshSchematic tempSchematic = m_MeshSchematics.at(GraphicObject->m_schematicName);

	if(result == m_Textures.end())
	{
		tempSchematic.maps[DetailMap] = AllocateNewTexture(TextureName, AbsolutePath + "Assets\\Meshes\\" + schematicName + "\\Diffuse\\" + TextureName);
		tempSchematic.mapNames[DetailMap] = TextureName;

		DoesExist(tempSchematic);

		m_MeshSchematics.insert(make_pair(tempSchematic.name, tempSchematic));
	}
	else
	{
		tempSchematic.maps[DetailMap] = result->second;
		tempSchematic.mapNames[DetailMap] = TextureName;

		DoesExist(tempSchematic);

		m_MeshSchematics.insert(make_pair(tempSchematic.name, tempSchematic));
	}

	//just incase the schematic name changed
	GraphicObject->m_schematicName = tempSchematic.name;
	GraphicObject->m_maps[DetailMap] = tempSchematic.maps[DetailMap];
}

void MemoryManager::SwapMesh(MantisMesh* GraphicObject, string MeshName, string fileName, string AbsolutePath)
{
	D3DXVECTOR3 origin = GraphicObject->m_position;

	Actor *tempActor = getActor(GraphicObject->m_owningActorID);

	DeleteMantisMesh(GraphicObject);

	MeshSchematic schematic = RegisterNewMeshSchematic(MeshName, fileName, AbsolutePath);

	GraphicObject = CreateMantisMesh(schematic);
	GraphicObject->m_position = origin;

	tempActor->Graphics = GraphicObject;

	Defragment();
}
//**************************************