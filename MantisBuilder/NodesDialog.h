#pragma once
#include "d3dUtil.h"

class NodesDialog
{
private:

	HWND dialogHWND;

public:
	NodesDialog(void);
	~NodesDialog(void);

	BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

