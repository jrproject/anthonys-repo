#pragma once

#include <d3dx9.h>
#include <string>
#include "Camera.h"

class SkyBox
{
public:
	SkyBox(const std::string& filename, float radius);
	~SkyBox();

	void onLostDevice();
	void onResetDevice();

	void render();

private:
	float m_radius;
	ID3DXMesh* m_sphere;
	IDirect3DCubeTexture9* m_cubeMap;
	ID3DXEffect* m_FX;
	D3DXHANDLE m_hTech;
	D3DXHANDLE m_hcube;
	D3DXHANDLE m_hWVP;

};

