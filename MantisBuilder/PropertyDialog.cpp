#include "PropertyDialog.h"
#include "WindowsDelegateMethods.h"
#include "WorkBench.h"
#include "resource.h"
#include <windowsx.h>
#include "Actor.h"
#include "MantisMesh.h"
#include "MemoryManager.h"

using namespace std;

PropertyDialog::PropertyDialog()
{
	//Graphics Subset
	m_Graphics		= false;
	m_Diffuse		= false;
	m_Bump			= false;
	m_Normal		= false;
	m_Illumination	= false;
	m_Specular		= false;

	//***** Subset
}


PropertyDialog::~PropertyDialog()
{
	DestroyWindow(m_dialogHWND);
}

BOOL CALLBACK PropertyDialog::DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
	switch(message)
	{
		case WM_INITDIALOG:
		{
			Init(hwndDlg);
			return TRUE;
		}

		case WM_COMMAND:
		{
			switch(LOWORD(wParam))
			{
				case IDC_CHECK1:
					ToggleGraphics();
					return TRUE;
				
				case IDC_CHECK_DIF:
					ToggleDiffuse();
					return TRUE;

				case IDC_CHECK_BMP:
					ToggleBump();
					return TRUE;
				
				case IDC_CHECK_NRM:
					ToggleNormal();
					return TRUE;
				
				case IDC_CHECK_ILL:
					ToggleIllumination();
					return TRUE;
				
				case IDC_CHECK_SPEC:
					ToggleSpecular();
					return TRUE;
				
				case IDCANCEL:
				{
					DestroyWindow(hwndDlg);
					WDM->ToggleProperty();
					delete this;
					return TRUE;
				}

				case IDC_COMBO_DIF:
				{
					switch(HIWORD(wParam))
					{
						case CBN_CLOSEUP:
						{
							PerformTextureSwap(m_DiffuseTextures, ComboBox_GetCurSel(m_DiffuseComboHWND), DIFFUSE_MAP);
							return TRUE;
						}
					}
					return TRUE;
				}
				
				case IDC_COMBO_BMP:
				{
					switch(HIWORD(wParam))
					{
						case CBN_CLOSEUP:
						{
							PerformTextureSwap(m_BumpTextures, ComboBox_GetCurSel(m_BumpComboHWND), BUMP_MAP);
							return TRUE;
						}
					}
					return TRUE;
				}
				
				case IDC_COMBO_NRM:
				{
					switch(HIWORD(wParam))
					{
						case CBN_CLOSEUP:
						{
							PerformTextureSwap(m_NormalTextures, ComboBox_GetCurSel(m_NormalComboHWND), NORMAL_MAP);
							return TRUE;
						}
					}
					return TRUE;
				}
				
				case IDC_COMBO_ILL:
				{
					switch(HIWORD(wParam))
					{
						case CBN_CLOSEUP:
						{
							PerformTextureSwap(m_IlluminationTextures, ComboBox_GetCurSel(m_IlluminationComboHWND), ILLUMINATION_MAP);
							return TRUE;
						}
					}
					return TRUE;
				}
								
				case IDC_COMBO_SPEC:
				{
					switch(HIWORD(wParam))
					{
						case CBN_CLOSEUP:
						{
							PerformTextureSwap(m_SpecularTextures, ComboBox_GetCurSel(m_SpecularComboHWND), SPECULAR_MAP);
							return TRUE;
						}
					}
					return TRUE;
				}
			}
			return TRUE;
		}
	}
	return FALSE;
} 

// Private Functions
//====================================

void PropertyDialog::Init(HWND hwnd)
{
	m_DiffuseComboHWND		= GetDlgItem(hwnd, IDC_COMBO_DIF);
	m_BumpComboHWND			= GetDlgItem(hwnd, IDC_COMBO_BMP);
	m_NormalComboHWND		= GetDlgItem(hwnd, IDC_COMBO_NRM);
	m_IlluminationComboHWND	= GetDlgItem(hwnd, IDC_COMBO_ILL);
	m_SpecularComboHWND		= GetDlgItem(hwnd, IDC_COMBO_SPEC);
	m_ShaderComboHWND		= GetDlgItem(hwnd, IDC_COMBO_SHADER);

	m_GraphicsCheckHWND		= GetDlgItem(hwnd, IDC_CHECK1);
	m_DiffuseCheckHWND		= GetDlgItem(hwnd, IDC_CHECK_DIF);
	m_BumpCheckHWND			= GetDlgItem(hwnd, IDC_CHECK_BMP);
	m_NormalCheckHWND		= GetDlgItem(hwnd, IDC_CHECK_NRM);
	m_IlluminationCheckHWND	= GetDlgItem(hwnd, IDC_CHECK_ILL);
	m_SpecularCheckHWND		= GetDlgItem(hwnd, IDC_CHECK_SPEC);

	m_MaterialRED			= GetDlgItem(hwnd, IDC_EDIT1);
	m_MaterialGREEN			= GetDlgItem(hwnd, IDC_EDIT11);
	m_MaterialBLUE			= GetDlgItem(hwnd, IDC_EDIT12);
	m_MaterialALPHA			= GetDlgItem(hwnd, IDC_EDIT2);
}

bool PropertyDialog::isChecked(HWND hwnd)
{
	LRESULT result = Button_GetCheck(hwnd);

	if(result == BST_CHECKED)
		return true;
	else
		return false;
}

//// Graphics Subset

void PropertyDialog::ToggleGraphics()
{
	if(m_Graphics)
	{
		Button_SetCheck(m_GraphicsCheckHWND, FALSE);
		
		ComboBox_Enable(m_ShaderComboHWND, FALSE);

		TurnDetailMapOFF(m_DiffuseComboHWND, m_DiffuseCheckHWND, m_Diffuse);

		TurnDetailMapOFF(m_BumpComboHWND, m_BumpCheckHWND, m_Bump);

		TurnDetailMapOFF(m_NormalComboHWND, m_NormalCheckHWND, m_Normal);

		TurnDetailMapOFF(m_IlluminationComboHWND, m_IlluminationCheckHWND, m_Illumination);

		TurnDetailMapOFF(m_SpecularComboHWND, m_SpecularCheckHWND, m_Specular);

		Edit_Enable(m_MaterialRED, FALSE);
		Edit_Enable(m_MaterialGREEN, FALSE);
		Edit_Enable(m_MaterialBLUE, FALSE);
		Edit_Enable(m_MaterialALPHA, FALSE);
	}
	else
	{
		Button_SetCheck(m_GraphicsCheckHWND, TRUE);

		ComboBox_Enable(m_ShaderComboHWND, TRUE);

		TurnDetailMapON(m_DiffuseComboHWND, m_DiffuseCheckHWND, m_Diffuse);

		TurnDetailMapON(m_BumpComboHWND, m_BumpCheckHWND, m_Bump);

		TurnDetailMapON(m_NormalComboHWND, m_NormalCheckHWND, m_Normal);

		TurnDetailMapON(m_IlluminationComboHWND, m_IlluminationCheckHWND, m_Illumination);

		TurnDetailMapON(m_SpecularComboHWND, m_SpecularCheckHWND, m_Specular);

		Edit_Enable(m_MaterialRED, TRUE);
		Edit_Enable(m_MaterialGREEN, TRUE);
		Edit_Enable(m_MaterialBLUE, TRUE);
		Edit_Enable(m_MaterialALPHA, TRUE);
	}

	m_Graphics = !m_Graphics;
}

void PropertyDialog::ToggleDiffuse()
{
	if(m_Diffuse)
	{
		DisableDetailMap(m_DiffuseComboHWND, m_Diffuse);
	}
	else
	{
		EnableDetailMap(m_DiffuseComboHWND, m_Diffuse);
	}
}

void PropertyDialog::ToggleBump()
{
	if(m_Bump)
	{
		DisableDetailMap(m_BumpComboHWND, m_Bump);
	}
	else
	{
		EnableDetailMap(m_BumpComboHWND, m_Bump);
	}
}

void PropertyDialog::ToggleNormal()
{
	if(m_Normal)
	{
		DisableDetailMap(m_NormalComboHWND, m_Normal);
	}
	else
	{
		EnableDetailMap(m_NormalComboHWND, m_Normal);
	}
}

void PropertyDialog::ToggleIllumination()
{
	if(m_Illumination)
	{
		DisableDetailMap(m_IlluminationComboHWND, m_Illumination);
	}
	else
	{
		EnableDetailMap(m_IlluminationComboHWND, m_Illumination);
	}
}

void PropertyDialog::ToggleSpecular()
{
	if(m_Specular)
	{
		DisableDetailMap(m_SpecularComboHWND, m_Specular);
	}
	else
	{
		EnableDetailMap(m_SpecularComboHWND, m_Specular);
	}
}

void PropertyDialog::TurnDetailMapOFF(HWND combo, HWND check, bool &status)
{
	ComboBox_Enable(combo, FALSE);
	Button_Enable(check, FALSE);
	Button_SetCheck(check, FALSE);
	status = false;
}

void PropertyDialog::TurnDetailMapON(HWND combo, HWND check, bool &status)
{
	ComboBox_Enable(combo, TRUE);
	Button_Enable(check, TRUE);
	Button_SetCheck(check, TRUE);
	status = true;
}

void PropertyDialog::EnableDetailMap(HWND combo, bool &status)
{
	ComboBox_Enable(combo, TRUE);
	status = true;
}

void PropertyDialog::DisableDetailMap(HWND combo, bool &status)
{
	ComboBox_Enable(combo, FALSE);
	status = false;
}

void PropertyDialog::PopulateDetailMap(HWND combo, HWND check, string spec, vector<string> &textureList, bool &status)
{
	RECT rect;
	int fileCount = 0;
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	textureList.clear();

	hFind = FindFirstFile(spec.c_str(), &ffd);

	do {
		if((string(ffd.cFileName) != ".") && (string(ffd.cFileName) != "..")){
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				//ignore folders
			}
			else {

				ComboBox_AddString(combo, ffd.cFileName);
				textureList.push_back(ffd.cFileName);
				fileCount++;
			}
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);

	if(fileCount == 0)
	{
		TurnDetailMapOFF(combo, check, status);
	}
	else
	{
		TurnDetailMapON(combo, check, status);

		ComboBox_SetCurSel(combo, 0);

		GetClientRect(combo, &rect);
		MapDialogRect(combo, &rect);
		SetWindowPos(combo, 0, 0, 0, rect.right, (fileCount + 1) * rect.bottom, SWP_NOMOVE);
	}
}

void PropertyDialog::PerformTextureSwap(vector<string> TextureList, int index, int targetDetail)
{
	MMI->SwapTexture(m_SelectedGraphic, TextureList.at(index), targetDetail, WDM->WorkingDirectory()+"\\");
}
////****************

//void PropertyDialog::UpdateAI(MantisAI* AIObject)
//{
//	//NYI
//}

void PropertyDialog::UpdateGraphics(MantisMesh* graphicsObject)
{
	if(graphicsObject == nullptr)
	{
		if(m_Graphics)
			ToggleGraphics();
		return;
	} 
	else 
	{
		if(!m_Graphics)
			ToggleGraphics();
	}

	m_SelectedGraphic = graphicsObject;

	ComboBox_ResetContent(m_DiffuseComboHWND);
	ComboBox_ResetContent(m_BumpComboHWND);
	ComboBox_ResetContent(m_NormalComboHWND);
	ComboBox_ResetContent(m_IlluminationComboHWND);
	ComboBox_ResetContent(m_SpecularComboHWND);

	string temp = graphicsObject->m_fileName;

	string basePath = WDM->WorkingDirectory() + "\\Assets\\Meshes\\" + temp;

	string spec = basePath + "\\Diffuse\\*";
	PopulateDetailMap(m_DiffuseComboHWND, m_DiffuseCheckHWND, spec, m_DiffuseTextures, m_Diffuse);

	spec = basePath + "\\Bump\\*";
	PopulateDetailMap(m_BumpComboHWND, m_BumpCheckHWND, spec, m_BumpTextures, m_Bump);

	spec = basePath + "\\Normal\\*";
	PopulateDetailMap(m_NormalComboHWND, m_NormalCheckHWND, spec, m_NormalTextures, m_Normal);

	spec = basePath + "\\Specular\\*";
	PopulateDetailMap(m_SpecularComboHWND, m_SpecularCheckHWND, spec, m_SpecularTextures, m_Specular);

	spec = basePath + "\\Illumination\\*";
	PopulateDetailMap(m_IlluminationComboHWND, m_IlluminationCheckHWND, spec, m_IlluminationTextures, m_Illumination);

	char buffer[255];
	sprintf_s(buffer, "%1.2f", graphicsObject->m_material.ambient.r);
	Edit_SetText(m_MaterialRED, buffer);
	sprintf_s(buffer, "%1.2f", graphicsObject->m_material.ambient.g);
	Edit_SetText(m_MaterialGREEN, buffer);
	sprintf_s(buffer, "%1.2f", graphicsObject->m_material.ambient.b);
	Edit_SetText(m_MaterialBLUE, buffer);
	sprintf_s(buffer, "%1.2f", graphicsObject->m_material.ambient.a);
	Edit_SetText(m_MaterialALPHA, buffer);
}

//void PropertyDialog::UpdatePhysics(MantisPhysics* physicsObject)
//{
//	//NYI
//}
//
//void PropertyDialog::UpdateGameplay(MantisGameplay* gameplayObject)
//{
//	//NYI
//}

//************************************

// Public Functions
//====================================

void PropertyDialog::Update()
{
	Actor* selectedActor = WBI->getSelectedActor();

	if(selectedActor == nullptr)
	{
		ToggleGraphics();
	}
	else
	{
		//UpdateAI(selectedActor->AI);
		UpdateGraphics(selectedActor->Graphics);
		//UpdatePhysics(selectedActor->Physics);
		//UpdateGameplay(selectedActor->Gameplay);
	}
}

//************************************