#include "AssetDialog.h"
#include "d3dApp.h"
#include "WindowsDelegateMethods.h"
#include "FileCreator.h"
#include "WorkBench.h"
#include <CommCtrl.h>

using namespace std;

AssetDialog::AssetDialog()
{
	
}


AssetDialog::~AssetDialog()
{
	DestroyWindow(m_dialogHWND);
}

// Public Functions
//===========================

BOOL CALLBACK AssetDialog::DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{
    switch (message) 
    { 
        case WM_INITDIALOG: 
			if(WDM->SolutionIsOpen())
			{
				DlgDirList(hwndDlg, (LPSTR)WDM->WorkingDirectory().c_str(), IDC_LIST1, 0, DDL_DIRECTORY);
				m_currentPath = WDM->WorkingDirectory();
			}
			return TRUE; 
 
        case WM_COMMAND: 
		{
            switch (LOWORD(wParam)) 
            { 
				
                case IDOK: 
 
                    return TRUE; 
 
                case IDCANCEL: 
                    DestroyWindow(hwndDlg); 
					WDM->ToggleAssets();
					delete this;
                    return TRUE; 

				case IDC_LIST1:
				{
					switch (HIWORD(wParam))
					{						
						case LBN_SELCHANGE:
						{
							char buffer[256];
							if(DlgDirSelectEx(hwndDlg, buffer, 255, IDC_LIST1))
							{
								if(!strcmp(buffer, "..\\"))
									
									upLevel();
								else
								{
									m_PreviousFolder = buffer;
									m_PreviousFolder.pop_back();
									forward(buffer);
								}

								DlgDirList(hwndDlg, buffer, IDC_LIST1, 0, DDL_DIRECTORY);
								
							}
							return TRUE;
						}

						case LBN_DBLCLK:
						{
							char buffer[256];
							DlgDirSelectEx(hwndDlg, buffer, 255, IDC_LIST1);
							WDM->NewPickedItem(m_relativePath + "\\" + buffer, m_PreviousFolder, buffer);
							return TRUE;
						}
					}
					return TRUE;
				}
			}    
			return TRUE;
		} 

		case WM_DROPFILES:
		{
			string buffer;
			UINT bufferSize;
			buffer.resize(256);
			FileCreator fileCreator;
			vector<string> IncomingFiles;

			UINT number = DragQueryFile((HDROP)wParam, 0xFFFFFFFF, (LPSTR)buffer.c_str(), 256);

			for(UINT i = 0; i < number; i++)
			{
				bufferSize = DragQueryFile((HDROP)wParam, i, NULL, 0) + 1;

				buffer.resize(bufferSize);

				DragQueryFile((HDROP)wParam, i, (LPSTR)buffer.c_str(), bufferSize);
				
				IncomingFiles.push_back(buffer);

				buffer.clear();
			}

			fileCreator.HandleFiles(IncomingFiles, m_currentPath + m_relativePath);

			DragFinish((HDROP)wParam);

			DlgDirList(hwndDlg, (LPSTR)(m_currentPath + m_relativePath).c_str(), IDC_LIST1, 0, DDL_DIRECTORY);

			return TRUE;
		}
    } 
    return FALSE; 
} 

//***************************

// Private Functions
//===========================

void AssetDialog::upLevel()
{
	for(auto it = m_relativePath.rbegin();
		it != m_relativePath.rend();
		it++)
	{
		if(*it == '\\')
		{
			it++;
			m_relativePath = string(m_relativePath.begin(), it.base());
			return;
		}
	}
}

void AssetDialog::forward(string newPath)
{
	newPath.pop_back();
	m_relativePath += "\\" + newPath;
}

//***************************