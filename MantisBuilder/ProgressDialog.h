#pragma once
#include "dialogwindow.h"
class ProgressDialog : public DialogWindow
{
public:
	ProgressDialog();
	~ProgressDialog();

	void SetOperationText(std::string text);
	void SetTaskText(std::string text);

	void SetItemCount(int count);
	
	void Step();

	void Close();

	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

