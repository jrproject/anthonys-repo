//=============================================================================
// HelloDirect3D.cpp by Frank Luna (C) 2005 All Rights Reserved.
//
// Demonstrates Direct3D Initialization and text output using the 
// framework code.
//=============================================================================

#include "WinMain.h"

#include "WindowsDelegateMethods.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	
	#if defined(DEBUG) | defined(_DEBUG)
	// Enable Console
		AllocConsole();

		HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
		int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
		FILE* hf_out = _fdopen(hCrt, "w");
		setvbuf(hf_out, NULL, _IONBF, 1);
		*stdout = *hf_out;

		HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
		hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
		FILE* hf_in = _fdopen(hCrt, "r");
		setvbuf(hf_in, NULL, _IONBF, 128);
		*stdin = *hf_in;
	
	// Enable run-time memory check for debug builds.
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	#endif

	//_CrtSetBreakAlloc(249);

	Game app(hInstance, "Mantis Builder 0.7.0", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;

	DirectInput di(DISCL_NONEXCLUSIVE|DISCL_FOREGROUND, DISCL_NONEXCLUSIVE|DISCL_FOREGROUND);
	gDInput = &di;



	//MantisSprite msi;
	//gSprite = &msi;

	app.startGSM();
	
	return gd3dApp->run();
}

Game::Game(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	srand(time_t(0));

	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}

	gXinput = new XBOXController(1);
	//m_xboxActive = true;
	m_GfxStats = new GfxStats();
	m_skyBox = new SkyBox("Assets/Meshes/Skybox/space_cube_02.dds", 20000.0f);
	
	gCamera->setSpeed(800);
	gCamera->setLens(D3DXToRadian(45), (float)md3dPP.BackBufferWidth /  (float)md3dPP.BackBufferHeight, 1.0f, 50000.0f);

	m_paused = false;

	gCamera->fixedDist() = 400;
	gCamera->lookAt(D3DXVECTOR3(50.0f, 100.0f, -50.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	gCamera->setInput(PC_MOUSEKEYBOARD);
	gCamera->setMode(FIXED_ROTATION);
}

Game::~Game()
{
	delete m_GfxStats;
	delete GSM;
	//delete m_HUD;
	delete gXinput;
	delete m_skyBox;
}

bool Game::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

void Game::onLostDevice()
{
	m_GfxStats->onLostDevice();
	m_skyBox->onLostDevice();

	//HR(gSprite->mSprite->OnLostDevice());
	GSM->OnLostDevice();
}

void Game::onResetDevice()
{
	m_GfxStats->onResetDevice();
	m_skyBox->onResetDevice();
	gCamera->onResetDevice((float)md3dPP.BackBufferWidth, (float)md3dPP.BackBufferHeight);

	//HR(gSprite->mSprite->OnResetDevice());
	GSM->OnResetDevice();
}

void Game::startGSM()
{
	GSM = new GameStateMachine();
	GSM->SetCurrentState(new LevelEditing(this));
	GSM->InitializeState();
}


GameStateMachine* Game::getGSM()
{
	return GSM;
}


void Game::updateScene(float dt)
{
	m_GfxStats->update(dt);
	gDInput->poll();
	EventXBOXController();
	gCamera->update(dt);

	if(gXinput->current[0].Start && (gXinput->previous[0].Start == false))
	{
		if(!m_paused)
		{
			m_paused = true;
			//GSM->ChangeState(new GameMenu(this));
		} else {
			m_paused = false;
			GSM->ReverToPreviousState();
		}
	}

	if(WDM->LevelIsLoaded())
	{	
		GSM->UpdateScene(dt);	
	}
}

void Game::drawScene()
{
	HR(gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x33333333, 1.0f, 0));

	HR(gd3dDevice->BeginScene());

	if(WDM->LevelIsLoaded())
	{	
		m_skyBox->render();
		m_GfxStats->display();
		GSM->RenderScene();
	}

	//HR(gSprite->mSprite->Begin(D3DXSPRITE_ALPHABLEND));

	//HR(gSprite->mSprite->Flush());
	//HR(gSprite->mSprite->End());

	HR(gd3dDevice->EndScene());

	HR(gd3dDevice->Present(0, 0, 0, 0));
}

void Game::EventXBOXController()
{
	lock.acquire();

	//if the thread is asleep don't poll
	//if(gXinput->ThreadKillState == 1)
	//{
	//	DXCI->setControllerMode(ControlModeMouse);
	//	return;
	//}

	//DXCI->setControllerMode(ControlModeXBOX);

	//check for fullscreen toggle
	if(gXinput->current[0].Select && (gXinput->previous[0].Select == false))
		enableFullScreenMode(md3dPP.Windowed);

	//send the controller structure to ControllerPoll
	//gMyGameWorld->ControllerPoll(gXinput->current[0], gXinput->previous[0]);

	gXinput->wait = false;
	

	lock.release();
}