#pragma once
#include "gamestate.h"
#include "AutoGrid.h"
#include "d3dUtil.h"

class Game;

class LevelEditing : public GameState
{
private:

	Game* m_owner;

	AutoGrid *grid;

	DirectionalLight m_light;
	MeshMaterial m_material;

	ID3DXEffect *m_FX;

	D3DXHANDLE m_tech;

	D3DXHANDLE m_WVP;
	D3DXHANDLE m_World;
	D3DXHANDLE m_WorldInv;

	D3DXHANDLE m_EyePos;

	D3DXHANDLE m_Tex;

	D3DXHANDLE m_Mtrl;
	D3DXHANDLE m_Light;

	LPDIRECT3DTEXTURE9 m_lineTexture;

private:

	void buildFX();

public:
	LevelEditing(Game *owner);
	~LevelEditing();

	virtual void InitializeState();
    virtual void UpdateScene(float dt);
    virtual void RenderScene();
    virtual void OnResetDevice();
    virtual void OnLostDevice();
    virtual void LeaveState();
};

