#pragma once
#include <vector>

#include "d3dUtil.h"
#include "DirectInput.h"
#include "HelperAnchor.h"

class Actor;

#define WBI WorkBench::Instance()

enum {
	TRANSFORM_NONE = 0,
	TRANSFORM_TRANSLATE,
	TRANSFORM_ROTATE,
	TRANSFORM_SCALE,
	TRANSFORM_END,
};

class MantisMesh;
class Game;

class WorkBench
{
private:

	WorkBench();

	void GetWorldPickingRay(D3DXVECTOR3 &originW, D3DXVECTOR3 &dirW);

	BOOL MouseIsOverlapping(MantisMesh* mesh, D3DXVECTOR3 &originW, D3DXVECTOR3 &dirW);
	bool ClickedOnMesh(MantisMesh *mesh);

	void RemoveFromSelected(MantisMesh *mesh);
	void ClearSelection();
	void AddToSelection(MantisMesh *mesh);
	void SubtractFromSelection(MantisMesh *mesh);

	void Transforms(float dt);

	void ApplyTranslation(float dt);
	void ApplyRotation(float dt);
	void ApplyScale(float dt);

	bool AnchorInteraction();

	void CheckCameraSnap();

	void NewSelectedActor();

private:
	Game *m_owningApp;
	std::vector<MantisMesh*> m_SelectedMeshes;

	int m_TransformMode;
	bool m_Transforming;
	bool m_DoubleClick;
	int m_axis;
	HelperAnchor *m_anchor;

	Actor *m_SelectedActor;

public:
	
	static WorkBench* Instance();
	~WorkBench();
	void Init();	//NYI
	void Unload();
	void Reset();

	void SetApplication(Game *game);
	void SetSelectedActor(Actor* newActor);

	void Update(float dt);

	void SetTransformMode(int mode);
	void NextTransformMode();

	Actor* getSelectedActor();



	bool DEBUGNOW;
};

