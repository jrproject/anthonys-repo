#include "AutoGrid.h"
#include "Vertex.h"
#include "Camera.h"

using namespace std;

AutoGrid::AutoGrid()
{

}

AutoGrid::AutoGrid(float gridSize, float density, float maxDistance)
{	
	ranges[0] = maxDistance;
	ranges[1] = maxDistance / 2;
	ranges[2] = ranges[1] / 3;
	ranges[3] = ranges[2] / 4;
	ranges[4] = ranges[3] / 5;

	m_maxDistance = maxDistance;
	halfSize = gridSize / 2.0f;
	increment = gridSize / density;
	m_gridSize = gridSize;
	den = density;
	float x = -halfSize;
	m_vertexCount = (int)(density * 4.0f) + 4;

	InitAllVertexDeclarations();

	gd3dDevice->CreateVertexBuffer((m_vertexCount+4)*sizeof(VertexPNT),
								   D3DUSAGE_WRITEONLY,
								   0,
								   D3DPOOL_MANAGED,
								   &m_VertexBuffer,
								   0);

	VertexPNT* verts = 0;

	m_VertexBuffer->Lock(0, 0, (void**)&verts, 0);

	for(int i = 0; i <= m_vertexCount; i += 4)
	{
		verts[i]	= VertexPNT(x, 0.0, halfSize, 0.0, 1.0, 0.0, 0.0, 1.0);
		verts[i+1]	= VertexPNT(x, 0.0, -halfSize, 0.0, 1.0, 0.0, 0.0, 1.0);

		verts[i+2]	= VertexPNT(halfSize, 0.0, x, 0.0, 1.0, 0.0, 0.0, 1.0);
		verts[i+3]	= VertexPNT(-halfSize, 0.0, x, 0.0, 1.0, 0.0, 0.0, 1.0);

		x += increment;
	}

	m_VertexBuffer->Unlock();
}

AutoGrid::~AutoGrid()
{
	DestroyAllVertexDeclarations();
}

void AutoGrid::Update()
{
	static int newDensity = 0, previousDensity = 0;
	static float previousDistance = gCamera->fixedDist();

	float distance = gCamera->fixedDist();

	if(previousDistance == distance)
		return;

	previousDistance = distance;

	if(distance >= m_maxDistance)
	{
		gCamera->fixedDist() = m_maxDistance;
		return;
	}

	int density = (int)den;

	for(int i = 5; i >= 0; i--)
	{
		if(distance <= ranges[i])
		{
			density = (int)den * (i+1);
			newDensity = i;
			break;
		}
	}

	if(newDensity == previousDensity)
		return;

	gCamera->setSpeed(1000 * (5.0f/(newDensity + 1.0f)));

	previousDensity = newDensity;

	ReleaseCOM(m_VertexBuffer);
	
	float x = -halfSize;
	increment = m_gridSize / density;
	m_vertexCount = (int)(density * 4.0f) + 4;

	gd3dDevice->CreateVertexBuffer((m_vertexCount+4)*sizeof(VertexPNT),
								   D3DUSAGE_WRITEONLY,
								   0,
								   D3DPOOL_MANAGED,
								   &m_VertexBuffer,
								   0);

	VertexPNT* verts = 0;

	m_VertexBuffer->Lock(0, 0, (void**)&verts, 0);

	for(int i = 0; i <= m_vertexCount; i += 4)
	{
		verts[i]	= VertexPNT(x, 0.0, halfSize, 0.0, 1.0, 0.0, 0.0, 1.0);
		verts[i+1]	= VertexPNT(x, 0.0, -halfSize, 0.0, 1.0, 0.0, 0.0, 1.0);

		verts[i+2]	= VertexPNT(halfSize, 0.0, x, 0.0, 1.0, 0.0, 0.0, 1.0);
		verts[i+3]	= VertexPNT(-halfSize, 0.0, x, 0.0, 1.0, 0.0, 0.0, 1.0);

		x += increment;
	}

	m_VertexBuffer->Unlock();
}

void AutoGrid::Render()
{
	gd3dDevice->SetStreamSource(0, m_VertexBuffer, 0, sizeof(VertexPNT));
	gd3dDevice->SetVertexDeclaration(VertexPNT::Decl);

	gd3dDevice->DrawPrimitive(D3DPT_LINELIST, 0, m_vertexCount/2);
}