#pragma once
#include "d3dUtil.h"
#include <map>

#define MMI MemoryManager::Instance()

using namespace std;

class MantisMesh;
class MantisAI;
class MantisPhysics;

class Actor;

// ========== Enumerations ==========
enum MeshType 
{
	SPHERE,
	BOX,
	CYLINDER,
};

enum MapNames 
{
	DIFFUSE_MAP,
	BUMP_MAP,
	NORMAL_MAP,
	SPECULAR_MAP,
	ILLUMINATION_MAP,
};

enum DetailFlags
{
	DIF = 0x00001,
	BMP = 0x00010,
	NRM = 0x00100,
	SPC = 0x01000,
	ILL = 0x10000,
};
// ==================================

enum ResourceType{
	GRAPHIC = 0,
};

class MemoryManager
{
private:

	MemoryManager();

private:

	bool m_memoryInitalized;

private:

	map<int, Actor*>						m_Actors;

	map<string, MeshSchematic>				m_MeshSchematics;

	map<string, LPDIRECT3DTEXTURE9>			m_Textures;
	map<string, ID3DXMesh*>					m_Meshes;

	map<string, vector<MantisMesh*>>		m_GraphicObjects;	
	map<string, vector<MantisAI*>>			m_AIObjects;
	map<string, vector<MantisPhysics*>>		m_PhysicsObjects;
	
	vector<vector<MantisMesh*>*>			m_AllGraphicObjects;
	vector<vector<MantisAI*>*>				m_AllAIObjects;
	vector<vector<MantisPhysics*>*>			m_AllPhysicsObjects;

	//======================================
	map<string, LPDIRECT3DTEXTURE9>			m_EDITOR_Textures;

	map<string, MeshSchematic>				m_EDITOR_MeshSchematics;		//EDITOR ONLY MEMORY
	map<string, vector<MantisMesh*>>		m_EDITOR_GraphicObjects;		
	vector<vector<MantisMesh*>*>			m_EDITOR_AllGraphicObjects;
	//======================================

public:
	static MemoryManager* Instance();
	~MemoryManager(void);

	//memory meta functions
	void InitMemory();
	void CleanUp();
	void IsMemoryGood();
	void ReloadMemory();
	void Defragment();
	//=====================

	//Utility Functions
	ID3DXMesh*			CalculateTBN(ID3DXMesh* mesh);
	//======================================

	//Resource Allocation
	LPDIRECT3DTEXTURE9	AllocateNewTexture(string resourceName, string resourcePath);
	ID3DXMesh*			AllocateNewMesh(string resourceName, string resourcePath);
	//======================================

	//Schematic Management
	void				RegisterNewMeshSchematic(string SchematicName, string filename, string details[5]);
	MeshSchematic		RegisterNewMeshSchematic(string SchematicName, string filename,  string AbsolutePath);
	void				RegisterNewMeshSchematic(MeshSchematic Schematic);				
	MeshSchematic		GetMeshSchematic(string SchematicName);

	bool				ContentsDoMatch(MeshSchematic Incoming, MeshSchematic Orginal);
	void				DoesExist(MeshSchematic &Schematic);				
	//======================================

	// Creation Functions
	MantisMesh*		CreateMantisMesh(string SchematicName);
	MantisMesh*		CreateMantisMesh(MeshSchematic Schematic);
	MantisAI*		CreateMantisAI(string AISchematic);
	MantisPhysics*	CreateMantisPhysics(string PhysicsSchematic);

	void			AddActor(Actor* newActor);
	//======================================

	// Delete Functions
	void			DeleteMantisMesh(MantisMesh* mesh);
	//======================================
	
	//Accessor functions
	vector<vector<MantisMesh*>*>		GetGraphicsObjects();
	vector<vector<MantisAI*>*>			GetAIObjects();
	vector<vector<MantisPhysics*>*>		GetPhysicsObjects();

	LPDIRECT3DTEXTURE9					GetTexture(string textureName);
	ID3DXMesh*							GetMesh(string meshName);

	Actor*								getActor(int id);
	//======================================

	

	//Level Editor Helper functions ONLY - Jesus Christ Marie they're minerals
	map<string, MeshSchematic> getSchematics();
	map<string, vector<MantisMesh*>> getGraphicsObjects();

	LPDIRECT3DTEXTURE9 AllocateNewEditorTexture(string resourceName, string resourcePath);

	void RegisterNewEditorMeshSchematic(string filename); 
	MantisMesh* CreateEditorMeshInstance(string SchematicName); 
	vector<vector<MantisMesh*>*> getEditorMeshes(); 

	void SwapTexture(MantisMesh* GraphicObject, string TextureName, int DetailMap, string AbsolutePath = "");
	void SwapMesh(MantisMesh* GraphicObject, string MeshName, string fileName, string AbsolutePath = "");
	//======================================
};

