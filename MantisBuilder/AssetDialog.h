#pragma once
#include "DialogWindow.h"

class AssetDialog : public DialogWindow
{
private:

	std::string m_currentPath;
	std::string m_relativePath;
	std::string m_PreviousFolder;

	void upLevel();
	void forward(std::string newPath);

public:
	AssetDialog();
	~AssetDialog(void);

	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);
};

