#pragma once
#include "d3dUtil.h"

class DialogWindow
{
protected:

	HWND m_dialogHWND;

public:
	DialogWindow() { m_dialogHWND = 0; }
	DialogWindow(HWND dialogHWND):m_dialogHWND(dialogHWND) {}
	virtual ~DialogWindow() { DestroyWindow(m_dialogHWND); }

	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam) = 0;

	HWND getHWND(){ return m_dialogHWND; }
	void setHWND(HWND hwnd) { m_dialogHWND = hwnd; }
};

