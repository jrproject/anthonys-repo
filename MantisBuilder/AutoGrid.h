#pragma once
#include "d3dUtil.h"
#include <vector>

class AutoGrid
{
private:

	LPDIRECT3DVERTEXBUFFER9 m_VertexBuffer;

	int m_vertexCount;
	float m_maxDistance;
	float halfSize;
	float increment;
	float den;
	float m_gridSize;

	float ranges[5];

private:

public:
	AutoGrid();
	AutoGrid(float gridSize, float density, float maxDistance);
	~AutoGrid();

	void Update();
	void Render();
};

