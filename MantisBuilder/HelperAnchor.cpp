#include "HelperAnchor.h"
#include "MemoryManager.h"
#include "MantisMesh.h"
#include "Camera.h"
#include "Vertex.h"

HelperAnchor::HelperAnchor()
{
	m_mode = MODE_TRANSLATE;
	m_attatchPoint = nullptr;

	LoadAssets();
}


HelperAnchor::~HelperAnchor()
{
	//Memory manager is managing our resources
	// C=
	m_anchors.clear();
	m_baises.clear();
	m_attatchPoint = nullptr;
}

// Priavte Functions
//==============================================
void HelperAnchor::LoadAssets()
{
	MMI->InitMemory();
	//MMI->RegisterNewMeshSchematic("arrow_head");
	MMI->RegisterNewEditorMeshSchematic("arrow_head");

	AnchorHelperElement anchorElement;

	//anchorElement.m_helper = MMI->CreateMeshInstance("arrow_head");
	anchorElement.m_helper = MMI->CreateEditorMeshInstance("arrow_head");
	anchorElement.m_direction = 'X';

	anchorElement.m_helper->m_material.diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	anchorElement.m_helper->m_material.ambient = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
	anchorElement.m_helper->m_material.spec = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
	anchorElement.m_helper->m_material.specPower = 50.0f;

	m_anchors.push_back(anchorElement);
	m_rotations.push_back(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_baises.push_back(D3DXVECTOR3(1.0f, 0.0f, 0.0f));

	//anchorElement.m_helper = MMI->CreateMeshInstance("arrow_head");
	anchorElement.m_helper = MMI->CreateEditorMeshInstance("arrow_head");
	anchorElement.m_direction = 'Y';
	m_anchors.push_back(anchorElement);
	m_rotations.push_back(D3DXVECTOR3(0.0f, 0.0f, 90.0f));
	m_baises.push_back(D3DXVECTOR3(0.0f, 1.0f, 0.0f));

	//anchorElement.m_helper = MMI->CreateMeshInstance("arrow_head");
	anchorElement.m_helper = MMI->CreateEditorMeshInstance("arrow_head");
	anchorElement.m_direction = 'Z';
	m_anchors.push_back(anchorElement);
	m_rotations.push_back(D3DXVECTOR3(-90.0f, 0.0f, 0.0f));
	m_baises.push_back(D3DXVECTOR3(0.0f, 0.0f, 1.0f));
}
//==============================================

// Public Functions
//==============================================
void HelperAnchor::Update()
{
	if(m_attatchPoint == nullptr) return;

	switch(m_mode)
	{
		case MODE_TRANSLATE:
		{
			for(int i = 0; i < 3; i++)
			{
					
				D3DXQuaternionNormalize(&m_anchors[i].m_helper->m_orientation, &m_anchors[i].m_helper->m_orientation);
				D3DXQuaternionRotationYawPitchRoll(&m_anchors[i].m_helper->m_orientation,
												   D3DXToRadian(m_rotations[i].x),
												   D3DXToRadian(m_rotations[i].y),
												   D3DXToRadian(m_rotations[i].z));

				m_anchors[i].m_helper->m_position = (*m_attatchPoint + m_baises[i] * (gCamera->fixedDist() / 10)/*scale*/);

				m_anchors[i].m_helper->m_scale = D3DXVECTOR3((gCamera->fixedDist() / 100), (gCamera->fixedDist() / 100), (gCamera->fixedDist() / 100));

				m_anchors[i].m_helper->m_material.diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
				m_anchors[i].m_helper->m_material.ambient = D3DXCOLOR(m_baises[i].x, m_baises[i].y, m_baises[i].z, 1.0f);
				m_anchors[i].m_helper->m_material.spec = D3DXCOLOR(m_baises[i].x, m_baises[i].y, m_baises[i].z, 1.0f);
				m_anchors[i].m_helper->m_material.specPower = 50.0f;

				m_anchors[i].m_helper->m_Sphere->radius = (gCamera->fixedDist() / 10);
	
			}
			break;
		}
	}
}

void HelperAnchor::SetMode(int newMode)
{

}

void HelperAnchor::SetAttachPoint(D3DXVECTOR3 *newPoint)
{
	m_attatchPoint = newPoint;
}

int HelperAnchor::ClickedOn(D3DXVECTOR3 &originW, D3DXVECTOR3 &dirW)
{
	int axis = 1;
	for(auto mesh : m_anchors)
	{
		D3DXMATRIX R, T, S;
		D3DXMatrixTranslation(&T, mesh.m_helper->m_position.x, mesh.m_helper->m_position.y, mesh.m_helper->m_position.z);
		D3DXMatrixScaling(&S, mesh.m_helper->m_scale.x, mesh.m_helper->m_scale.y, mesh.m_helper->m_scale.z);

		D3DXMATRIX toWorld = S * T;

		AABB box;
		mesh.m_helper->m_AABB->xform(toWorld, box);

		D3DXVECTOR3 transformedPoint;
		D3DXVec3TransformCoord(&transformedPoint, &mesh.m_helper->m_Sphere->pos, &toWorld);

		if(D3DXSphereBoundProbe(&transformedPoint, mesh.m_helper->m_Sphere->radius, &originW, &dirW))
			return axis;
		else 
			axis++;
	}

	return 0;
}
//==============================================