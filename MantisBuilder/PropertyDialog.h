#pragma once
#include "DialogWindow.h"

class MantisMesh;

class PropertyDialog : public DialogWindow
{
private:

	void Init(HWND hwnd);

	bool isChecked(HWND hwnd);

	//Graphics Subset
	void ToggleGraphics();
	void ToggleDiffuse();
	void ToggleBump();
	void ToggleNormal();
	void ToggleIllumination();
	void ToggleSpecular();

	void TurnDetailMapOFF(HWND combo, HWND check, bool &status);
	void TurnDetailMapON(HWND combo, HWND check, bool &status);

	void EnableDetailMap(HWND combo, bool &status);
	void DisableDetailMap(HWND combo, bool &status);

	void PopulateDetailMap(HWND combo, HWND check, std::string path, std::vector<std::string> &textureList, bool &status);

	void PerformTextureSwap(std::vector<std::string> TextureList, int index, int targetDetail);

	//void UpdateAI(MantisAI* AIObject);
	void UpdateGraphics(MantisMesh* graphicsObject);
	//void UpdatePhysics(MantisPhysics* physicsObject);
	//void UpdateGameplay(MantisGameplay* gameplayObject);

private:

	MantisMesh* m_SelectedGraphic;

	//Graphics Check box handels
	HWND m_GraphicsCheckHWND,
		 m_DiffuseCheckHWND,
		 m_BumpCheckHWND,
		 m_NormalCheckHWND,
		 m_IlluminationCheckHWND,
		 m_SpecularCheckHWND;

	//Graphics Combo box Handels
	HWND m_DiffuseComboHWND,
		 m_BumpComboHWND,
		 m_NormalComboHWND,
		 m_IlluminationComboHWND,
		 m_SpecularComboHWND,
		 m_ShaderComboHWND;

	HWND m_MaterialRED,
		 m_MaterialGREEN,
		 m_MaterialBLUE,
		 m_MaterialALPHA;
		 
	bool m_Graphics,
		 m_Diffuse,
		 m_Bump,
		 m_Normal,
		 m_Illumination,
		 m_Specular;

	std::vector<std::string> m_DiffuseTextures;
	std::vector<std::string> m_BumpTextures;
	std::vector<std::string> m_NormalTextures;
	std::vector<std::string> m_IlluminationTextures;
	std::vector<std::string> m_SpecularTextures;

public:
	PropertyDialog();
	~PropertyDialog();

	virtual BOOL CALLBACK DialogProc(HWND hwndDlg, UINT messge, WPARAM wParam, LPARAM lParam);

	void Update();
};

