#pragma once
#include "d3dApp.h"
#include <tchar.h>
#include <crtdbg.h>
#include "GfxStats.h"
#include "GameStateMachine.h"
#include "DirectInput.h"
#include "Camera.h"
#include "XBOXController.h"
#include "LevelEditing.h"
#include "SkyBox.h"


class Game : public D3DApp
{
	friend class WorkBench;
public:
	Game(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~Game();

	bool checkDeviceCaps();
	void onLostDevice();
	void onResetDevice();
	void updateScene(float dt);
	void drawScene();

	void startGSM();
	void startHUD();

	GameStateMachine *getGSM();

	GfxStats			*m_GfxStats;

private:
	GameStateMachine	*GSM;

	Camera				*m_Camera;
	SkyBox				*m_skyBox;

	bool				m_paused;

	void EventXBOXController();
};