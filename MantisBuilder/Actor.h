#pragma once
#include "d3dApp.h"
#include "MantisMesh.h"

class MantisPhysics;
class MantisAI;
class MantisGameplay;

class Actor
{
public:

	MantisAI		*AI;
	MantisMesh		*Graphics;
	MantisPhysics	*Physics;
	MantisGameplay	*Gameplay;

	int ID;
	D3DXVECTOR3		Pos;

public:

	Actor()
	{
		AI			= nullptr;
		Graphics	= nullptr;
		Physics		= nullptr;
		Gameplay	= nullptr;

		Pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		static int nextID = 0;
		ID = nextID;
		nextID++;
	}

	~Actor()
	{
		//delete AI;
		delete Graphics;
		//delete Physics;
		//delete Gameplay;
	}
};