#include "NewDialog.h"
#include "d3dApp.h"
#include "WindowsDelegateMethods.h"

NewDialog::NewDialog()
{
}


NewDialog::~NewDialog(void)
{
}

BOOL CALLBACK NewDialog::DialogProc(HWND hwndDlg, 
                             UINT message, 
                             WPARAM wParam, 
                             LPARAM lParam) 
{ 
    switch (message) 
    { 
        case WM_INITDIALOG: 
		{
            return TRUE; 
		}
 
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
            { 
                case IDOK: 
				{
					HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST1);

					int lbItem = (int)SendMessage(hwndList, LB_GETCURSEL, 0, 0);
					int i = (int)SendMessage(hwndList, LB_GETITEMDATA, lbItem, 0);

					HWND hwndEdit1 = GetDlgItem(hwndDlg, IDC_EDIT1);
					HWND hwndEdit2 = GetDlgItem(hwndDlg, IDC_EDIT2);

					std::string name, description;
					int value = (int)SendMessage(hwndEdit1, EM_LINELENGTH, 0, 0);
					name.resize(value);

					//value = (int)SendMessage(hwndEdit2, EM_LINELENGTH, 0, 0);
					//description.resize(value);

					char buffer[256];

					SendMessage(hwndEdit1, EM_GETLINE, 0, (LPARAM)buffer);

					//SendMessage(hwndEdit2, EM_GETLINE, 0, (LPARAM)description.c_str());

					name = std::string(buffer);
					name.resize(value);

					WDM->NewSolution(name);

					DestroyWindow(hwndDlg); 
                    WDM->ToggleNew();
					delete this;
                    return TRUE; 
				}
 
                case IDCANCEL: 
                    DestroyWindow(hwndDlg); 
                    WDM->ToggleNew();
					delete this;
                    return TRUE; 
            } 
    } 
    return FALSE; 
} 
