#include "LevelEditing.h"
#include "WinMain.h"
#include "Camera.h"
#include "WindowsDelegateMethods.h"
#include "WorldManager.h"
#include "MantisMesh.h"
#include "WorkBench.h"

using namespace std;

LevelEditing::LevelEditing(Game *owner)
{
	m_owner = owner;
}

LevelEditing::~LevelEditing(void)
{
	delete grid;
	WMI->CleanUp();
	WBI->Unload();
}

//float intersectionCoordsToMesh(MantisMesh *mesh, D3DXVECTOR2 vec)
//{
//	D3DXVECTOR3 ray;
//
//	//Getting screen width / height needs to be abstracted to:
//	//Engine->GetScreenHeight();
//	//Engine->GetScreenWidth();
//	D3DDEVICE_CREATION_PARAMETERS params;
//	RECT rect;
//	gd3dDevice->GetCreationParameters(&params);
//	GetWindowRect(params.hFocusWindow, &rect);
//	///////////////////////////////////////////
//
//	int w = rect.right;
//	int h = rect.bottom;
//
//	D3DXMATRIX proj = gCamera->proj();
//
//	float x = (2.0f*s.x/w - 1.0f) / proj(0,0);
//	float y = (-2.0f*s.y/h + 1.0f) / proj(1,1);
//
//	// Build picking ray in view space.
//	D3DXVECTOR3 origin(0.0f, 0.0f, 0.0f), originW;
//	D3DXVECTOR3 dir(x, y, 1.0f), dirW;
//
//	// So if the view matrix transforms coordinates from 
//	// world space to view space, then the inverse of the
//	// view matrix transforms coordinates from view space
//	// to world space.
//	D3DXMATRIX invView;
//	D3DXMatrixInverse(&invView, 0, &gCamera->view());
//
//	// Transform picking ray to world space.
//	D3DXVec3TransformCoord(&originW, &origin, &invView);
//	D3DXVec3TransformNormal(&dirW, &dir, &invView);
//	D3DXVec3Normalize(&dirW, &dirW);
//
//
//	D3DXSphereBoundProbe(&mesh->m_position, 20.0f, &originW, &dirW);
//
//	BOOL hit = 0;
//	float dist = 0.0f;
//
//	D3DXIntersect(mesh->m_mesh, &originW, &dirW, &hit, NULL, NULL, NULL, &dist, NULL, NULL);
//
//	if(hit) return dist;
//	else return -1.0f;
//}

void LevelEditing::InitializeState()
{
	buildFX();

	WBI->SetApplication(m_owner);

	grid = new AutoGrid(18000, 12, 10000);

	m_light.ambient = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_light.diffues = D3DXCOLOR(0.5f, 0.5f, 0.0f, 1.0f);
	m_light.specular = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_light.directionWorld = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

	m_material.ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	m_material.diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_material.spec = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f);
	m_material.specPower = 50.0f;

	D3DXCreateTextureFromFile(gd3dDevice, "Assets/Meshes/Colors/Solids/white.jpg", &m_lineTexture);

	HR(m_FX->SetTechnique(m_tech));

	HR(m_FX->SetValue(m_Mtrl, &m_material, sizeof(MeshMaterial)));
	HR(m_FX->SetValue(m_Light, &m_light, sizeof(DirectionalLight)));

	m_FX->SetTexture(m_Tex, m_lineTexture);
}

void LevelEditing::UpdateScene(float dt)
{
	grid->Update();
	WBI->Update(dt);
}

void LevelEditing::RenderScene()
{
	
	D3DXMATRIX World, WorldInverse;

	//m_FX->SetValue(m_Light, &m_light, sizeof(PointLight));
	m_FX->SetValue(m_EyePos, gCamera->pos(), sizeof(D3DXVECTOR3));
	
	D3DXMatrixIdentity(&World);

	m_FX->SetMatrix(m_World, &World);
	m_FX->SetMatrix(m_WVP,  &(World * gCamera->viewProj()));

	D3DXMatrixInverse(&WorldInverse, 0, &World);
	m_FX->SetMatrix(m_WorldInv, &WorldInverse);

	

	UINT numPasees = 0;
	HR(m_FX->Begin(&numPasees, 0));
	HR(m_FX->BeginPass(0));

	//===============================
	m_material.specPower = 0.0f;
	m_material.spec = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	HR(m_FX->SetValue(m_Mtrl, &m_material, sizeof(MeshMaterial)));
	m_FX->SetTexture(m_Tex, m_lineTexture);

	m_material.spec = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f);
	m_material.specPower = 50.0f;

	m_FX->CommitChanges();

	grid->Render();

	HR(m_FX->SetValue(m_Mtrl, &m_material, sizeof(MeshMaterial)));

	//===============================

	vector<vector<MantisMesh*>*> m_Drawables = WMI->getDrawableMeshes();
	
	for(vector<vector<MantisMesh*>*>::iterator arrayType = m_Drawables.begin();
		arrayType != m_Drawables.end();
		arrayType++)
	{
		DWORD renderFlags = (*arrayType)->at(0)->m_renderFlags;

		/*if(renderFlags & BMP) {
			m_FX->SetTexture(m_Bump, (*arrayType)->at(0)->m_maps[BUMP_MAP]);
			m_FX->SetBool(m_FBmp, true);
		} else {
			m_FX->SetBool(m_FBmp, false);
		}

		if(renderFlags & NRM) {
			m_FX->SetTexture(m_Norm, (*arrayType)->at(0)->m_maps[NORMAL_MAP]);
			m_FX->SetBool(m_FNrm, true);
		} else {
			m_FX->SetBool(m_FNrm, false);
		}

		if(renderFlags & SPC) {
			m_FX->SetTexture(m_Specular, (*arrayType)->at(0)->m_maps[SPECULAR_MAP]);
			m_FX->SetBool(m_FSpc, true);
		} else {
			m_FX->SetBool(m_FSpc, false);
		}

		if(renderFlags & ILL) {
			m_FX->SetTexture(m_Illumination, (*arrayType)->at(0)->m_maps[ILLUMINATION_MAP]);
			m_FX->SetBool(m_FIll, true);
		} else {
			m_FX->SetBool(m_FIll, false);
		}*/

		for(vector<MantisMesh*>::iterator it = (*arrayType)->begin();
			it != (*arrayType)->end();
			it++)
		{
			(*it)->buildTransformMatrix();

			D3DXMatrixIdentity(&World);
			World = World * (*it)->m_transformMatrix;

			m_FX->SetMatrix(m_World, &World);
			m_FX->SetMatrix(m_WVP, &(World * gCamera->viewProj()));

			D3DXMatrixInverse(&WorldInverse, 0, &World);
			m_FX->SetMatrix(m_WorldInv, &WorldInverse);

			//if(renderFlags & DIF){
				m_FX->SetTexture(m_Tex, (*it)->m_maps[DIFFUSE_MAP]);
				HR(m_FX->SetValue(m_Mtrl, &(*it)->m_material, sizeof(MeshMaterial)));
				//m_FX->SetBool(m_FDif, true);
			//} else {
				//m_FX->SetBool(m_FDif, false);
			//}

			m_FX->CommitChanges();

			(*it)->m_mesh->DrawSubset(0);
		}
	}

	//===== EDITOR MESHES ===============

	gd3dDevice->SetRenderState(D3DRS_ZENABLE, FALSE);

	vector<vector<MantisMesh*>*> m_EditorMeshes = MMI->getEditorMeshes();
	
	for(vector<vector<MantisMesh*>*>::iterator arrayType = m_EditorMeshes.begin();
		arrayType != m_EditorMeshes.end();
		arrayType++)
	{
		DWORD renderFlags = (*arrayType)->at(0)->m_renderFlags;

		/*if(renderFlags & BMP) {
			m_FX->SetTexture(m_Bump, (*arrayType)->at(0)->m_maps[BUMP_MAP]);
			m_FX->SetBool(m_FBmp, true);
		} else {
			m_FX->SetBool(m_FBmp, false);
		}

		if(renderFlags & NRM) {
			m_FX->SetTexture(m_Norm, (*arrayType)->at(0)->m_maps[NORMAL_MAP]);
			m_FX->SetBool(m_FNrm, true);
		} else {
			m_FX->SetBool(m_FNrm, false);
		}

		if(renderFlags & SPC) {
			m_FX->SetTexture(m_Specular, (*arrayType)->at(0)->m_maps[SPECULAR_MAP]);
			m_FX->SetBool(m_FSpc, true);
		} else {
			m_FX->SetBool(m_FSpc, false);
		}

		if(renderFlags & ILL) {
			m_FX->SetTexture(m_Illumination, (*arrayType)->at(0)->m_maps[ILLUMINATION_MAP]);
			m_FX->SetBool(m_FIll, true);
		} else {
			m_FX->SetBool(m_FIll, false);
		}*/

		for(vector<MantisMesh*>::iterator it = (*arrayType)->begin();
			it != (*arrayType)->end();
			it++)
		{
			(*it)->buildTransformMatrix();

			D3DXMatrixIdentity(&World);
			World = World * (*it)->m_transformMatrix;

			m_FX->SetMatrix(m_World, &World);
			m_FX->SetMatrix(m_WVP, &(World * gCamera->viewProj()));

			D3DXMatrixInverse(&WorldInverse, 0, &World);
			m_FX->SetMatrix(m_WorldInv, &WorldInverse);

			//if(renderFlags & DIF) {
				m_FX->SetTexture(m_Tex, (*it)->m_maps[DIFFUSE_MAP]);
				HR(m_FX->SetValue(m_Mtrl, &(*it)->m_material, sizeof(MeshMaterial)));
				//m_FX->SetBool(m_FDif, true);
			//} else {
				//m_FX->SetBool(m_FDif, false);
			//}

			m_FX->CommitChanges();

			(*it)->m_mesh->DrawSubset(0);
		}
	}

	gd3dDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

	//=============================================

	HR(m_FX->EndPass());
	HR(m_FX->End());
}

void LevelEditing::OnResetDevice()
{
	HR(m_FX->OnResetDevice());
}

void LevelEditing::OnLostDevice()
{
	HR(m_FX->OnLostDevice());
}

void LevelEditing:: LeaveState()
{
	ReleaseCOM(m_FX);

	delete grid;
}

//===== Specific Functions =========================

void LevelEditing::buildFX()
{
	LPD3DXBUFFER XLog = 0;
	HR(D3DXCreateEffectFromFile(gd3dDevice, "PhongShader.fx", 0, 0, 0, 0, &m_FX, &XLog));
	if(XLog)
		 MessageBox(0, (char*)XLog->GetBufferPointer(), 0, 0);

	//technique
	m_tech			= m_FX->GetTechniqueByName("PhongDirLtTexTech");

	//transform and view matracies
	m_WVP			= m_FX->GetParameterByName(0, "gWVP");
	m_World			= m_FX->GetParameterByName(0, "gWorld");
	m_WorldInv		= m_FX->GetParameterByName(0, "gWorldInv");

	//camera eye
	m_EyePos		= m_FX->GetParameterByName(0, "gEyePos");

	//parallax settings
	//m_HeightScale	= m_FX->GetParameterByName(0, "gHeightScale");

	//material handles
	m_Mtrl			= m_FX->GetParameterByName(0, "gMtrl");
	m_Light			= m_FX->GetParameterByName(0, "gLight");

	//texture handles
	m_Tex			= m_FX->GetParameterByName(0, "gTex");
	//m_Norm			= m_FX->GetParameterByName(0, "gNorm");
	//m_Specular		= m_FX->GetParameterByName(0, "gSpecular");
	//m_Illumination	= m_FX->GetParameterByName(0, "gIllumination");
	//m_Bump			= m_FX->GetParameterByName(0, "gBump");
}

//==================================================